#!/bin/bash

set -eu
set -o pipefail

Bin=$(ls -tr ../target/*.jar | tail -n 1)

InPdbDir=./pdb/
OutAifDir=./out/
RefDir=./ref/


rm -rf ${OutAifDir} && mkdir ${OutAifDir}

function importPdb() {
local InPdb=$1
local OutFile=$2

java -jar ${Bin} \
        import_pdb_contacts \
        --pdb ${InPdb} \
        --distance 4.0 \
        --atomPattern 'C.*' \
        --ignoreIntraResidue \
        export_aif \
        -o ${OutFile} 
        sed -i '1d' ${OutFile}
        sort ${OutFile} -o ${OutFile}
}

InPdbs=$(find ${InPdbDir} -mindepth 1 -maxdepth 1 -name "*.pdb") 

i=1
N=$(echo ${InPdbs} | tr ' ' '\n' | wc -l)
for InPdb in ${InPdbs}
do
        echo "Importing pdb ${i} of ${N}"
        i=$((i+1))
        PdbCode=$(basename ${InPdb} .pdb)
        echo "${PdbCode}"
        OutFile=${OutAifDir}/${PdbCode}.aif
        importPdb ${InPdb} ${OutFile}
done

Error=$?

if [[ ${Error} -eq 0 ]]
then
        echo "OK"
else
        echo "Error: Exported .aif files not equal"
fi
