#!/bin/bash

set -eu
set -o pipefail

PdbCodesUrl=ftp://ftp.wwpdb.org/pub/pdb/derived_data/index/author.idx
PdbStructUrlBase=https://files.rcsb.org/download/
PdbCodes=$(curl -s ${PdbCodesUrl} | sed '1,/^ *$/d' | awk '{print $1}')
PdbCodesArr=(${PdbCodes})
Max=$(echo ${PdbCodes} | tr ' ' '\n' | wc -l)
Min=1
Range=$((${Max}-${Min}+1))
Pdbs=1000
DataDir=pdb

rm -rf ${DataDir} && mkdir ${DataDir}

function getRandomPdbCode() {
    local Rand=$((${RANDOM}+${Min}))
    echo ${PdbCodesArr[${Rand}]}
}



for i in $(seq 1 ${Pdbs})
do
    echo "Downloading number ${i} of ${Pdbs}"
    PdbCode=$(getRandomPdbCode)
    echo ${PdbCode}
    PdbUrl=${PdbStructUrlBase}/${PdbCode}.pdb
    PdbOut=${DataDir}/${PdbCode}.pdb
    curl -s ${PdbUrl} > ${PdbOut}
done

    
