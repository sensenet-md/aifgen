#!/bin/bash

set -eux
set -o pipefail

Bin=$(ls -tr ../target/*.jar | tail -n 1)

AifFile="./resources/aif/test.zaif"

OutFile=out/test.out.aif

java -jar ${Bin} \
        import_zaif \
        -i ${AifFile} \
        export_aif \
        -o ${OutFile}
