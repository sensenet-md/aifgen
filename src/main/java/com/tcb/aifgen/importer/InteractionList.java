package com.tcb.aifgen.importer;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.log.NotReported;
import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;

public class InteractionList {
		
	public static Integer getTimelineLength(Collection<Interaction> interactions){
		return ListFilter.singleton(
				interactions.stream()
				.map(i -> i.getTimeline().getLength())
				.collect(Collectors.toSet()))
				.orElseThrow(() -> new RuntimeException("Timeline lengths not equal in interactions"));
	}
		
	public static Set<Atom> getInteractingAtoms(Collection<Interaction> interactions) {
		Set<Atom> atoms = interactions.stream()
				.map(i -> Arrays.asList(i.getSourceAtom(),i.getTargetAtom()))
				.flatMap(l -> l.stream())
				.collect(Collectors.toSet());
		return atoms;	
	}
	
	public static Set<String> getChains(Collection<Interaction> interactions){
		return getInteractingResidues(interactions).stream()
				.map(r -> r.getChain())
				.collect(Collectors.toSet());
	}

	public static Set<Residue> getInteractingResidues(Collection<Interaction> interactions) {
		Set<Residue> residues =  getInteractingAtoms(interactions).stream()
				.map(a -> a.getResidue())
				.collect(Collectors.toSet());
		return residues;
	}
		
	public static Set<String> getInteractionTypes(Collection<Interaction> interactions){
		return interactions.stream()
				.map(i -> i.getType())
				.collect(Collectors.toSet());
	}
}
