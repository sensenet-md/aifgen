package com.tcb.aifgen.importer.aifImporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.compress.archivers.ArchiveException;

import com.tcb.aifgen.importer.TimelineType;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.util.TempUtil;
import com.tcb.cytoscape.cyLib.util.ZipUtil;
import com.tcb.common.util.Rounder;

public class ZaifWriter extends AifWriter {
	
	public ZaifWriter(List<Interaction> interactions, TimelineType type, Integer decimalPlaces){
		super(interactions,type,decimalPlaces);
	}
	
	public ZaifWriter(List<Interaction> interactions, TimelineType type){
		super(interactions,type);
	}
	
	public ZaifWriter(List<Interaction> interactions){
		super(interactions,TimelineType.TIMELINE);
	}
	
	@Override
	public void write(Path path) throws IOException {
		TempUtil tmpUtil = new TempUtil("zaif");
		Path tmpPath = tmpUtil.createTempFile();
		super.write(tmpPath);
		try{
			ZipUtil.compressSingleFile(tmpPath, path);
		} catch(ArchiveException e){
			throw new IOException(e);
		}
		tmpUtil.clean();
	}
	
	
}
