package com.tcb.aifgen.importer.aifImporter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipFile;

import org.apache.commons.lang3.EnumUtils;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.cytoscape.cyLib.util.TempUtil;
import com.tcb.cytoscape.cyLib.util.ZipUtil;
import com.tcb.monitor.Monitor;
import com.tcb.monitor.MonitorFormatImpl;
import com.tcb.monitor.MonitorImpl;
import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.aifgen.importer.aifImporter.record.RecordType;
import com.tcb.aifgen.importer.timeline.StringListTimelineFactory;
import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.aifgen.util.MapUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class AifImporter extends AbstractImporter implements InteractionImporter {

	private static final int dataTypeIndex = 0;
	private static final int interactionTypeIndex = 1;
	private static final int sourceAtomNameIndex = 2;
	private static final int targetAtomNameIndex = 3;
	private static final int sourceResIdIndex = 4;
	private static final int targetResIdIndex = 5;
	private static final int sourceResNameIndex = 6;
	private static final int targetResNameIndex = 7;
	private static final int sourceResInsertIndex = 8;
	private static final int targetResInsertIndex = 9;
	private static final int sourceAltLocIndex = 10;
	private static final int targetAltLocIndex = 11;
	private static final int sourceChainIndex = 12;
	private static final int targetChainIndex = 13;
	private static final int bridgeAtomNameIndex = 14;
	private static final int dataIndex = 15;
	
	public static final String commentChar = "#";
	public static final String fieldDelimiter = ",";
	public static final String timelineFieldDelimiter = " ";
	public static final String bridgeFieldDelimiter = " ";

	public static final String bridgeFieldDelimiterPattern = "\\s+";
	
	private static final Integer majorVersion = 1;
	private static final Integer minorVersion = 0;
		
	private Path path;
	private StringListTimelineFactory timelineFactory;
	private Integer sieve;
	private Double minAvg;

	public AifImporter(Path path, Integer sieve, Double minAvg){
		this.path = path;
		this.timelineFactory = new StringListTimelineFactory();
		this.sieve = sieve;
		this.minAvg = minAvg;
	}
	
	public AifImporter(Path path){
		this(path,1,Double.NEGATIVE_INFINITY);
	}
			
	@Override
	public InteractionImportData read() throws IOException {

		Map<RecordType,List<List<String>>> recordMap = readRecordMap();

		checkVersion(recordMap);
		
		List<List<String>> interactionLines = getInteractionLines(recordMap);

		List<Interaction> interactions = interactionLines.stream()
				.parallel()
				.map(l -> parseInteraction(l))
				.collect(Collectors.toList());
		interactions = filter(interactions,sieve,minAvg);
		
		TimelineType timelineType = getTimelineType(recordMap);
		
		return InteractionImportData.create(
				interactions,
				timelineType);
	}
	
	private void checkVersion(Map<RecordType,List<List<String>>> recordMap){
		RecordType key = RecordType.VERSION;
		if(!recordMap.containsKey(key)) return;
		List<List<String>> versionLines = recordMap.get(key);
		if(versionLines.size() > 1) throw new IllegalArgumentException("Found more than one version line");
		List<String> versionLine = versionLines.get(0);
		String version = versionLine.get(1);
		String[] versionFields = version.split("\\.");
		if (versionFields.length != 2) throw new IllegalArgumentException("Error during version parsing");
		Integer majorVersion = null;
		Integer minorVersion = null;
		try{
			 majorVersion = Integer.parseInt(versionFields[0]);
			 minorVersion = Integer.parseInt(versionFields[1]);
		} catch(NumberFormatException e){
			throw new IllegalArgumentException("Error during version parsing");
		}
		if(majorVersion > AifImporter.majorVersion)
			throw new IllegalArgumentException("Cannot read files with major version > "
		+ AifImporter.majorVersion.toString());
	}
	
	private Interaction parseInteraction(List<String> line){
		Interaction interaction = Interaction.create(
				getAtom(
						line, sourceChainIndex,
						sourceResIdIndex, sourceResNameIndex,
						sourceResInsertIndex, sourceAltLocIndex,
						sourceAtomNameIndex),
				getAtom(
						line, targetChainIndex,
						targetResIdIndex, targetResNameIndex,
						targetResInsertIndex, targetAltLocIndex,
						targetAtomNameIndex),
				getBridgeAtoms(line),
				timelineFactory.create(line.get(dataIndex)),
				line.get(interactionTypeIndex));
		return interaction;
	}
	
	private TimelineType getTimelineType(Map<RecordType,List<List<String>>> recordMap){
		Set<String> recordTypeNames = recordMap.keySet().stream()
				.map(r -> r.name())
				.collect(ImmutableSet.toImmutableSet());
		Set<String> timelineTypeNames = Stream.of(TimelineType.values())
				.map(t -> t.name())
				.collect(ImmutableSet.toImmutableSet());
		Set<String> commonTypes = Sets.intersection(recordTypeNames, timelineTypeNames);
		
		TimelineType timelineType = 
				TimelineType.valueOf(
					ListFilter.singleton(
							commonTypes)
					.orElseThrow(() -> new IllegalArgumentException("Cannot use more than one timeline type"))
					);
		return timelineType;
	}
	
	private Map<RecordType,List<List<String>>> readRecordMap() throws IOException {
		List<List<String>> lines = parseLines(path);
		return getRecordMap(lines);
	}
	
	private Map<RecordType,List<List<String>>> getRecordMap(List<List<String>> lines){
		Map<RecordType,List<List<String>>> result = new SafeMap<>();
		for(List<String> line:lines){
			String recordTag = line.get(0);
			RecordType recordType = EnumUtils.getEnum(RecordType.class, recordTag);
			if(recordType==null) throw new IllegalArgumentException("Unknown record type: " + recordTag);
			List<List<String>> recordLines = MapUtil.getOrPut(result, recordType, ArrayList::new);
			recordLines.add(line);
		}
		return result;
	}
	
	private List<List<String>> getInteractionLines(Map<RecordType,List<List<String>>> recordMap){
		boolean containsTimeline = recordMap.containsKey(RecordType.TIMELINE);
		boolean containsDiffTimeline = recordMap.containsKey(RecordType.DIFFERENCE_TIMELINE);
		if(containsTimeline && containsDiffTimeline)
			throw new IllegalArgumentException("Cannot use timeline and difference timeline records together");
		if(containsTimeline) return recordMap.get(RecordType.TIMELINE);
		if(containsDiffTimeline) return recordMap.get(RecordType.DIFFERENCE_TIMELINE);
		return new ArrayList<>();
	}
	
	
		
	private List<Atom> getBridgeAtoms(List<String> line){
		List<Atom> result = new ArrayList<>();
		String bridgeAtomNamesField = line.get(bridgeAtomNameIndex).trim();
		if(bridgeAtomNamesField.isEmpty()) return result;
		String[] bridgeAtomNames = bridgeAtomNamesField.split(bridgeFieldDelimiterPattern);
		for(String bridgeAtomName:bridgeAtomNames){
			Residue residue = getResidue(line, sourceChainIndex,
					sourceResIdIndex,sourceResNameIndex,
					sourceResInsertIndex, sourceAltLocIndex);
			Atom atom = Atom.create(bridgeAtomName, residue);
			result.add(atom);
		}
		return result;				
	}
	
	private List<String> splitTrim(String line){
		String[] split = line.split(fieldDelimiter);
		List<String> result = Arrays.stream(split)
				.map(s -> s.trim())
				.collect(Collectors.toList());
		return result;
	};
	
	protected List<List<String>> parseLines(Path path) throws IOException {
		List<List<String>> result = parseLines(new FileInputStream(path.toFile()));
		return result;
	}
	
	protected List<List<String>> parseLines(InputStream in) throws IOException {
		BufferedReader rdr = new BufferedReader(new InputStreamReader(in));
		try{
			List<List<String>> result = rdr.lines()
					.filter(l -> !l.startsWith(commentChar))
					.filter(l -> !l.trim().isEmpty())
					.map(l -> splitTrim(l))
					.collect(Collectors.toList());
			return result;
		} catch(Exception e){
			throw e;
		} finally {
			if(rdr!=null) rdr.close();
		}
	}
	
	private Atom getAtom(
			List<String> line,
			Integer chainIndex,
			Integer residIndex, 
			Integer resNameIndex,
			Integer resInsertIndex,
			Integer altLocIndex,
			Integer atomNameIndex){
		String name = line.get(atomNameIndex);
		Residue residue = getResidue(
				line, chainIndex,
				residIndex, resNameIndex,
				resInsertIndex, altLocIndex);
		Atom atom = Atom.create(name, residue);
		return atom;
	}
		
	private Residue getResidue(
			List<String> line,
			Integer chainIndex,
			Integer idIndex,
			Integer nameIndex,
			Integer resInsertIndex,
			Integer altLocIndex){
		String name = line.get(nameIndex);
		Integer id = Integer.valueOf(line.get(idIndex));
		String chain = getChain(line, chainIndex);
		String resInsert = line.get(resInsertIndex);
		String altLoc = line.get(altLocIndex);
		Residue residue = Residue.create(id, name, resInsert, altLoc, chain);
		return residue;
	}
	
	private String getChain(List<String> line, Integer chainIndex){
		String name = line.get(chainIndex);
		return name;
	}
	
	@Override
	public String getName() {
		return getMainFilePath().getFileName().toString();
	}
	
	private Path getMainFilePath() {
		return path;
	}

	



}
