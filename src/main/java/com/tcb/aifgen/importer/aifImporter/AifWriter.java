package com.tcb.aifgen.importer.aifImporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import com.tcb.aifgen.importer.TimelineType;
import com.tcb.aifgen.importer.aifImporter.record.RecordType;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;
import com.tcb.common.util.Rounder;

public class AifWriter {
	private List<Interaction> interactions;
	private TimelineType type;
	private Integer decimalPlaces;
	
	private static final Integer majorVersion = 1;
	private static final Integer minorVersion = 0;

	public AifWriter(List<Interaction> interactions, TimelineType type, Integer decimalPlaces){
		this.interactions = interactions;
		this.type = type;
		this.decimalPlaces = decimalPlaces;
	}
	
	public AifWriter(List<Interaction> interactions, TimelineType type){
		this(interactions,type,5);
	}
	
	public AifWriter(List<Interaction> interactions){
		this(interactions,TimelineType.TIMELINE);
	}
	
	public void write(Path path) throws IOException {
		Writer out = null;
		try{
			out = new BufferedWriter(new OutputStreamWriter(
		              new FileOutputStream(path.toString()), "utf-8"));
			out.write(getVersionRecord());
			for(Interaction interaction:interactions){
				String line = formatInteraction(interaction);
				out.write(line + "\n");
			}
		} catch(IOException e){
			throw(e);
		} finally {
			if(out!=null) out.close();
		}		
	}
	
	private String getVersionRecord(){
		return String.format("%s,%d.%d\n", RecordType.VERSION.name(), majorVersion, minorVersion);
	}
	
	private String formatInteraction(Interaction interaction){
		String interactionType = interaction.getType();
		Atom source = interaction.getSourceAtom();
		Atom target = interaction.getTargetAtom();
		Residue sourceResidue = source.getResidue();
		Residue targetResidue = target.getResidue();
		String sourceAtomName = source.getName();
		String targetAtomName = target.getName();
		Integer sourceResId = sourceResidue.getIndex();
		Integer targetResId = targetResidue.getIndex();
		String sourceResName = sourceResidue.getName();
		String targetResName = targetResidue.getName();
		String sourceResInsert = sourceResidue.getResidueInsert();
		String targetResInsert = targetResidue.getResidueInsert();
		String sourceAltLoc = sourceResidue.getAltLoc();
		String targetAltLoc = targetResidue.getAltLoc();
		String sourceChain = sourceResidue.getChain();
		String targetChain = targetResidue.getChain();
		String dataType = type.name();
		String timeline = formatTimeline(interaction.getTimeline());
		String bridgingNames = formatBridgingAtoms(interaction.getBridgingAtoms());
		String line = String.join(
				AifImporter.fieldDelimiter,
				Arrays.asList(
						dataType,
						interactionType,sourceAtomName,targetAtomName,
						sourceResId.toString(),targetResId.toString(),
						sourceResName,targetResName,
						sourceResInsert,targetResInsert,
						sourceAltLoc,targetAltLoc,
						sourceChain,targetChain,
						bridgingNames,
						timeline));
		return line;
	}
	
	private String formatBridgingAtoms(List<Atom> atoms){
		StringBuilder s = new StringBuilder();
		for(Atom a:atoms){
			s.append(a.getName());
			s.append(AifImporter.bridgeFieldDelimiter);
		}
		return s.toString();
	}
	
	private String formatTimeline(Timeline timeline){
		StringBuilder s = new StringBuilder();
		for(Double d:timeline.getData()){
			d = Rounder.round(d, decimalPlaces);
			s.append(d.toString());
			s.append(AifImporter.timelineFieldDelimiter);
		}
		return s.toString();		
	}
}
