package com.tcb.aifgen.importer.aifImporter;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.common.util.SafeMap;
import com.google.common.collect.ImmutableList;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class OldMatImporter extends AbstractImporter implements InteractionImporter {

	private static final int keyIndex = 0;
	private static final int sourceAtomIndex = 1;
	private static final int hydrogenAtomIndex = 2;
	private static final int targetAtomIndex = 3;
	private static final int sourceResNameIndex = 4;
	private static final int sourceResIdIndex = 5;
	private static final int sourceChainIndex = 6;
	private static final int sourceAtomNameIndex = 7;
	private static final int hydrogenAtomNameIndex = 8;
	private static final int targetResNameIndex = 9;
	private static final int targetResIdIndex = 10;
	private static final int targetChainIndex = 11;
	private static final int targetAtomNameIndex = 12;
	private static final int weightIndex = 13;
	private static final int timelineIndex = 14;
	
	private static final String interactionType = InteractionType.HBOND.toString();
		
	private Path path;

	public OldMatImporter(Path path){
		this.path = path;
	}
	
	
	@Override
	public InteractionImportData read() {
		List<List<String>> lines;
		
		try {
			lines = parseLines(path);
		} catch(IOException e) {throw new RuntimeException("Could not load file");}	
		
		ImmutableList.Builder<Interaction> interactions = ImmutableList.builder();
		for(List<String> line:lines){
			String key = line.get(keyIndex);
			// TODO Temporary fix for interaction types
			key = interactionType + "#" + key;
			interactions.add(Interaction.create(
					getAtom(line, sourceChainIndex, sourceResIdIndex, sourceResNameIndex, sourceAtomIndex, sourceAtomNameIndex),
					getAtom(line, targetChainIndex, targetResIdIndex, targetResNameIndex, targetAtomIndex, targetAtomNameIndex),
					Arrays.asList(
							getAtom(line, sourceChainIndex, sourceResIdIndex,
									sourceResNameIndex, hydrogenAtomIndex, hydrogenAtomNameIndex)),
					StringTimelineFactory.create(line.get(timelineIndex)),
					interactionType
					));
		}
		
		return InteractionImportData.create(
				interactions.build(),
				getTimelineType());
		
	}
	
	private List<List<String>> parseLines(Path path) throws IOException {
		BufferedReader lines;
		List<List<String>> result = new ArrayList<List<String>>();

		lines = Files.newBufferedReader(path);

		
		while(lines.ready()){
			String line = lines.readLine();
			if(line.startsWith("#")) continue;
			List<String> splitLine = Arrays.asList(line.trim().split("\\s+"));
			result.add(splitLine);
		}
		lines.close();
		
		return result;
	}
	
	private Atom getAtom(List<String> line, Integer chainIndex, Integer residIndex, Integer resNameIndex,
			Integer atomIdIndex, Integer atomNameIndex){
		Integer atomId = Integer.valueOf(line.get(atomIdIndex));
		String name = line.get(atomNameIndex);
		String element = null;
		
		Residue residue = getResidue(line, chainIndex, residIndex, resNameIndex);
		Atom atom = Atom.create(name, residue);
		return atom;
	}
		
	private Residue getResidue(List<String> line, Integer chainIndex, Integer idIndex, Integer nameIndex){
		String name = line.get(nameIndex);
		Integer id = Integer.valueOf(line.get(idIndex));
		String chain = getChain(line, chainIndex);
		String resInsert = "";
		String altLoc = "";
		Residue residue = Residue.create(id, name, resInsert, altLoc, chain);
		return residue;
	}
	
	private String getChain(List<String> line, Integer chainIndex){
		String name = line.get(chainIndex);
		return name;
	}
		
	private TimelineType getTimelineType() {
		return TimelineType.TIMELINE;
	}

	@Override
	public String getName() {
		return getMainFilePath().getFileName().toString();
	}
	
	private Path getMainFilePath() {
		return path;
	}
		

}
