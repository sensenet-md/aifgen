package com.tcb.aifgen.importer.aifImporter.record;

public enum RecordType {
	VERSION, TIMELINE, DIFFERENCE_TIMELINE;
}
