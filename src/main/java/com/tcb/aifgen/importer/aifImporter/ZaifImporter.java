package com.tcb.aifgen.importer.aifImporter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.cytoscape.cyLib.util.TempUtil;
import com.tcb.cytoscape.cyLib.util.ZipUtil;
import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.aifgen.importer.timeline.StringListTimelineFactory;
import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class ZaifImporter extends AifImporter {

	public ZaifImporter(Path path){
		super(path);
	}
	
	public ZaifImporter(Path path, Integer sieve, Double minAvg){
		super(path,sieve,minAvg);
	}
		
	@Override
	protected List<List<String>> parseLines(Path path) throws IOException {
		ZipFile f = new ZipFile(path.toFile());
		InputStream in = ZipUtil.decompressSingleFile(f);
		List<List<String>> result = parseLines(in);
		f.close();
		return result;
	}



}
