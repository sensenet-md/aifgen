package com.tcb.aifgen.importer.combinedImporter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.tcb.common.exception.TooManyMatchesException;
import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;
import com.google.common.collect.ImmutableList;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.aifgen.util.MapUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.log.NotReported;

public class CombinedInteractionImporter implements InteractionImporter {

	private List<InteractionImporter> subImporters;
		
	public CombinedInteractionImporter(List<InteractionImporter> interactionImporters){
		this.subImporters = interactionImporters;
	}
		
	@Override
	public InteractionImportData read() throws IOException {
		List<InteractionImportData> imports = readImporters();		
		
		TimelineType timelineType = ListFilter.singleton(imports.stream()
				.map(i -> i.getTimelineType())
				.collect(Collectors.toSet()))
				.orElseThrow(
						() -> new IllegalArgumentException(
								"Can only combine interactions of a single timeline type"));
		
		List<Interaction> interactions = createTimelinePaddedInteractions(imports);
						
		Map<Residue,Tuple<String,String>> mutationMap = imports.stream()
				.map(i -> i.getMutationMap())
				.reduce(new SafeMap<>(), MapUtil::merge);
		
		Map<Residue,String> secondaryStructures = imports.stream()
				.map(i -> i.getSecondaryStructureMap())
				.reduce(new SafeMap<>(), MapUtil::merge);
		
		return InteractionImportData.create(
				interactions,
				timelineType,
				mutationMap,
				secondaryStructures);
	}
	
	private List<InteractionImportData> readImporters() throws IOException {
		ImmutableList.Builder<InteractionImportData> imports = ImmutableList.builder();
		for(InteractionImporter i:subImporters){
			imports.add(i.read());
		}
		return imports.build();
	}
	
	
	
	private Integer checkTimelinesAndGetMaxLength(Collection<InteractionImportData> imports){
		Set<Integer> timelineLengths = imports.stream()
				.map(i -> i.getInteractions())
				.map(i -> InteractionList.getTimelineLength(i))
				.collect(Collectors.toSet());
		if(timelineLengths.size()==1) return ListFilter.singleton(timelineLengths).get();
		timelineLengths.remove(1);
		if(timelineLengths.size()!=1) throw new IllegalArgumentException(
				"Imported interactions must have the same timeline lengths or have a length of one");
		return ListFilter.singleton(timelineLengths).get();
	}
	
	private List<Interaction> expandedTimelines(List<Interaction> interactions, int maxLength){
		List<Interaction> result = new ArrayList<>();
		for(Interaction interaction:interactions){
				Timeline timeline = interaction.getTimeline();
				if(timeline.getLength().equals(1)){
					Interaction newInteraction = Interaction.create(
							interaction.getSourceAtom(),
							interaction.getTargetAtom(),
							interaction.getBridgingAtoms(),
							expandTimeline(timeline, maxLength),
							interaction.getType());
					result.add(newInteraction);
				} else {
					result.add(interaction);
				}
		}
		return result;
	}
	
	private Timeline expandTimeline(Timeline base, int length){
		if(base.getLength()!=1)	
			throw new IllegalArgumentException("Can only expand timelines of length 1");
		List<Double> data = new ArrayList<>();
		double v = base.getData()[0];
		for(int i=0;i<length;i++){
			data.add(v);
		}
		Timeline timeline = Timeline.create(data);
		return timeline;
	}
				
	private List<Interaction> createTimelinePaddedInteractions(Collection<InteractionImportData> imports) {
		List<Interaction> interactionsRaw = imports.stream()
				.map(i -> i.getInteractions())
				.flatMap(l -> l.stream())
				.collect(Collectors.toList());
		Integer timelineLength = checkTimelinesAndGetMaxLength(imports);
		List<Interaction> interactions = expandedTimelines(
				interactionsRaw, timelineLength);
		return interactions;
	}
	
	@Override
	public String getName() {
		return "(" + 
				subImporters.stream()
				.map(i -> i.getName())
				.reduce((s1,s2) -> s1 + "+" + s2)
				.get()
				+ ")";
	}

}
