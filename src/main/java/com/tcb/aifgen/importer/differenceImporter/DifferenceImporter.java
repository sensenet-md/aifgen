package com.tcb.aifgen.importer.differenceImporter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.aifgen.importer.combinedImporter.CombinedInteractionImporter;
import com.tcb.aifgen.util.MapUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.atoms.AtomLocation;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.difference.DifferenceInteractionFactory;
import com.tcb.atoms.interactions.difference.DummyInteractionFactory;
import com.tcb.atoms.interactions.predicates.UndirectedInteractionLocationEquivalence;
import com.tcb.atoms.residues.Residue;
import com.tcb.atoms.residues.ResidueLocation;
import com.tcb.cytoscape.cyLib.log.NotReported;

public class DifferenceImporter implements InteractionImporter {

	private InteractionImporter reference;
	private InteractionImporter compared;
		
	private static final BiPredicate<Interaction,Interaction> isLocationEquivalentFun = 
			new UndirectedInteractionLocationEquivalence();

	public DifferenceImporter(InteractionImporter reference, InteractionImporter compared){
		this.reference = reference;
		this.compared = compared;
	}

	@Override
	public InteractionImportData read() throws IOException {
		InteractionImportData referenceImport = reference.read();
		InteractionImportData comparedImport = compared.read();
		
		verifyTimelineTypes(referenceImport,comparedImport);
				
		List<Interaction> refInteractions = referenceImport.getInteractions();
		List<Interaction> comparedInteractions = comparedImport.getInteractions();
		
		Set<Residue> refResidues = InteractionList.getInteractingResidues(refInteractions);
		Set<Residue> comparedResidues = InteractionList.getInteractingResidues(comparedInteractions);
				
		Map<Residue,Tuple<String,String>> mutationMap = createMutationMap(refResidues,comparedResidues);
		
		List<Interaction> diffInteractions = createDiffInteractions(
				refInteractions,comparedInteractions, refResidues);
				
		Map<Residue,String> secondaryStructureMap = MapUtil.merge(
				referenceImport.getSecondaryStructureMap(), comparedImport.getSecondaryStructureMap());
		
		return InteractionImportData.create(
				diffInteractions, getTimelineType(),
				mutationMap, secondaryStructureMap);
	}
	
	private void verifyTimelineTypes(InteractionImportData referenceImport, InteractionImportData comparedImport){
		if(
				referenceImport.getTimelineType().equals(TimelineType.DIFFERENCE_TIMELINE) ||
				comparedImport.getTimelineType().equals(TimelineType.DIFFERENCE_TIMELINE)){
			throw new IllegalArgumentException("Cannot create difference networks of difference timelines");
		}
	}
		
	private Map<Residue,Tuple<String,String>> createMutationMap(
			Set<Residue> refResidues, Set<Residue> comparedResidues){
		Map<Residue,Tuple<String,String>> mutationMap = new SafeMap<Residue,Tuple<String,String>>();
				
		for(Residue ref:refResidues){
			for(Residue comp:comparedResidues){
				if(ResidueLocation.equalLocation(ref,comp)){
					String refName = ref.getName();
					String compName = comp.getName();
					if(!refName.equals(compName)) {
						mutationMap.put(ref, new Tuple<>(refName,compName));
					}
				}
			}
		}
					
		return mutationMap;
	}
	
	private Optional<Residue> getResidueWithLocation(Residue loc, Set<Residue> residues){
		List<Residue> equalLocationResidues = residues.stream()
				.filter(r -> ResidueLocation.equalLocation(loc, r))
				.collect(ImmutableList.toImmutableList());
		return ListFilter.singleton(equalLocationResidues);
	}
	
	private Interaction setToRefResidues(Interaction interaction, Set<Residue> residues){
		Atom source = interaction.getSourceAtom();
		Atom target = interaction.getTargetAtom();
		
		Residue sourceResidue = source.getResidue();
		Residue targetResidue = target.getResidue();
		
		Optional<Residue> sourceLocationEquivalentResidue = getResidueWithLocation(sourceResidue,residues);
		Optional<Residue> targetLocationEquivalentResidue = getResidueWithLocation(targetResidue,residues);
				
		if(sourceLocationEquivalentResidue.isPresent()){
			source = Atom.create(source.getName(), sourceLocationEquivalentResidue.get());
		}
		
		if(targetLocationEquivalentResidue.isPresent()){
			target = Atom.create(target.getName(), targetLocationEquivalentResidue.get());
		}
				
		List<Atom> oldBridgeAtoms = interaction.getBridgingAtoms();
		ImmutableList.Builder<Atom> bridgeAtoms = ImmutableList.builder();
		for(int i=0;i<oldBridgeAtoms.size();i++){
			Atom oldBridgeAtom = oldBridgeAtoms.get(i);
			Optional<Residue> bridgeResidueLoc = getResidueWithLocation(oldBridgeAtom.getResidue(),residues);
			if(bridgeResidueLoc.isPresent()){
				bridgeAtoms.add(Atom.create(oldBridgeAtom.getName(), bridgeResidueLoc.get()));
			} else {
				bridgeAtoms.add(oldBridgeAtom);
			}
		}
		
		return Interaction.create(
				source, target,
				bridgeAtoms.build(),
				interaction.getTimeline(),
				interaction.getType());
		
	}
		
	private List<Interaction> createDiffInteractions(
			List<Interaction> refInteractions,
			List<Interaction> comparedInteractions,
			Set<Residue> refResidues){
		ImmutableList.Builder<Interaction> diffInteractions = ImmutableList.builder();
		
		Set<Interaction> comparedInteractionsSet = new HashSet<>(comparedInteractions);
				
		for(Interaction ref:refInteractions){
			List<Interaction> equivalent = getEquivalent(ref,comparedInteractions);
			Interaction c = null;
			if(equivalent.size() > 1) throw new RuntimeException("Could not map residues unambigously");
			if(equivalent.isEmpty()) c = DummyInteractionFactory.create(ref);
			if(equivalent.size()==1) {
				c = equivalent.get(0);
				comparedInteractionsSet.remove(c);
			}
			Interaction diff = DifferenceInteractionFactory.create(ref,c);
			
			diffInteractions.add(diff);
		}
		
		for(Interaction comp:comparedInteractionsSet){
			Interaction c = DummyInteractionFactory.create(comp);
			c = setToRefResidues(c,refResidues);
			Interaction diff = DifferenceInteractionFactory.create(c,comp);
			
			diffInteractions.add(diff);
		}
		
		return diffInteractions.build();
	}
		
	private List<Interaction> getEquivalent(Interaction ref, Collection<Interaction> interactions){
		List<Interaction> lst = new ArrayList<>();
		for(Interaction i:interactions){
			if(isLocationEquivalentFun.test(ref, i)) lst.add(i);
		}
		return lst;
	}
					
	private TimelineType getTimelineType() {
		return TimelineType.DIFFERENCE_TIMELINE;
	}
			
	@Override
	public String getName(){
		return compared.getName() + "-" + reference.getName() + "-diff";
	}

}
