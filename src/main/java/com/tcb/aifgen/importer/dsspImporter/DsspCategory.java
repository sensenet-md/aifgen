package com.tcb.aifgen.importer.dsspImporter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum DsspCategory {
	HELIX,SHEET,OTHER;
	
	private static final Set<String> helixCategory = new HashSet<String>(Arrays.asList("G","H","I"));
	private static final Set<String> sheetCategory = new HashSet<String>(Arrays.asList("E","B"));
		
	public static DsspCategory fromString(String s){
		if(helixCategory.contains(s)) return HELIX;
		if(sheetCategory.contains(s)) return SHEET;
		
		return OTHER;
	}
	
}
