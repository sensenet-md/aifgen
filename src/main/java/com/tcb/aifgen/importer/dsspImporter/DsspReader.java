package com.tcb.aifgen.importer.dsspImporter;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tcb.aifgen.util.ResidueNameUtil;
import com.tcb.common.util.SafeMap;

public class DsspReader {
	private Path path;

	public DsspReader(Path path){
		this.path = path;
	}
	
	public List<Map<String,String>> readEntries() throws IOException {
		List<String> lines = getEntryLines();
		List<Map<String,String>> entries = new ArrayList<>();
		for(String line:lines){
			Map<String,String> map = new SafeMap<String,String>();
			map.put("SEQRES", strippedSubstring(line,0,5));
			map.put("PDBSEQ", strippedSubstring(line,5,10));
			map.put("iCode", strippedSubstring(line,10,11));
			map.put("CHAIN", strippedSubstring(line,11,12));
			map.put("AA", ResidueNameUtil.oneToThreeLetter(strippedSubstring(line,13,14)));
			map.put("S", strippedSubstring(line,16,17));
			entries.add(map);
		}
		return entries;		
	}
	
	private String strippedSubstring(String s, int lowInc, int highExc){
		return s.substring(lowInc, highExc).trim();
	}
	
	private List<String> getEntryLines() throws IOException {
		List<String> entries = new ArrayList<>();
		BufferedReader reader = Files.newBufferedReader(path);
		while(reader.ready()){
			String line = reader.readLine();
			if(isEntryLine(line)){
				entries.add(line);
			}
		}
		reader.close();
		return entries;
	}
	
	private boolean isEntryLine(String line){
		String trimmed = line.trim();
		if(trimmed.endsWith(".") ||
		   trimmed.startsWith("#") || 
		   trimmed.isEmpty()){
			return false;
		} else {
			return true;
		}
	}
}
