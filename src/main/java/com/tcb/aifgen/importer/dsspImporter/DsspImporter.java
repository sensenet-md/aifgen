package com.tcb.aifgen.importer.dsspImporter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.tcb.common.util.SafeMap;
import com.google.common.collect.ImmutableList;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.log.NotReported;

public class DsspImporter extends AbstractImporter {
		
	private static Set<DsspCategory> validCategories = 
			new HashSet<>(Arrays.asList(DsspCategory.HELIX, DsspCategory.SHEET));
	private static String defaultInteractionType = InteractionType.SECSTRUCT.toString();	
	
	private Path dsspPath;
	private String interactionType;
	
	public DsspImporter(Path dsspPath, String interactionType){
		this.dsspPath = dsspPath;
		this.interactionType = interactionType;
	}
	
	public DsspImporter(Path dsspPath){
		this(dsspPath,defaultInteractionType);
	}
	
	@Override
	public InteractionImportData read() throws IOException {
		List<Map<String,String>> entries = new DsspReader(dsspPath).readEntries();
		Map<Integer,Atom> atoms = new SafeMap<>();
		Map<Integer,String> secStructs = new SafeMap<>();
		
		for(int i=0;i<entries.size();i++){
			Map<String,String> entry = entries.get(i);
			String atomName = "CA";
			String altLoc = "";
			Integer resIndex = Integer.valueOf(entry.get("PDBSEQ"));
			String resInsert = entry.get("iCode");
			String chain = entry.get("CHAIN");
			String resName = entry.get("AA");
			String secStruct = entry.get("S");
						
			Atom atom = Atom.create(atomName, resIndex, resName, resInsert, altLoc, chain);
			atoms.put(i, atom);
			secStructs.put(i, secStruct);
		}
		
		List<Interaction> interactions = createInteractions(atoms,secStructs);		
			
		Map<Residue,String> secondaryStructures = createSecondaryStructures(atoms,secStructs);
		
		return InteractionImportData.create(
				interactions,
				getTimelineType(),
				new SafeMap<>(), secondaryStructures);
	}
	
	private List<Interaction> createInteractions(Map<Integer,Atom> atoms, Map<Integer,String> secStructs){
		ImmutableList.Builder<Interaction> interactions = ImmutableList.builder();
		for(Integer i:atoms.keySet()){
			Integer next = i+1;
			Atom atom = atoms.get(i);
			String secStruct = secStructs.get(i);
			String nextSecStruct = secStructs.getOrDefault(next, "");
			
			if(secStruct.isEmpty()) continue;
			if(!secStruct.equals(nextSecStruct)) continue;
			if(!validCategories.contains(DsspCategory.fromString(secStruct))) continue;
			
			Atom nextAtom = atoms.get(next);
			Interaction interaction = Interaction.create(
						atom,nextAtom,new ArrayList<Atom>(),
						Timeline.create(Arrays.asList(1)), interactionType);
			interactions.add(interaction);
							
		}
		return interactions.build();
	}
	
	private Map<Residue,String> createSecondaryStructures(Map<Integer,Atom> atoms, Map<Integer,String> secStructs){
		Map<Residue,String> secondaryStructures = new SafeMap<>();
		for(Integer i:atoms.keySet()){
			Atom atom = atoms.get(i);
			String secStruct = secStructs.get(i);
			Residue residue = atom.getResidue();
			if(!secondaryStructures.containsKey(residue)){
				secondaryStructures.put(residue, secStruct);
			}
		}
		return secondaryStructures;
	}
	
	private TimelineType getTimelineType() {
		return TimelineType.TIMELINE;
	}

	@Override
	public String getName() {
		return getMainFilePath().getFileName().toString();
	}
	
	private Path getMainFilePath() {
		return dsspPath;
	}
		
	public static String getDefaultInteractionType(){
		return defaultInteractionType;
	}
}
