package com.tcb.aifgen.importer;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.log.ParameterReporter;
import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;

@AutoValue
public abstract class InteractionImportData implements Serializable,ParameterReporter {
	
	private static final long serialVersionUID = 1l;
	
	public static InteractionImportData create(
			List<Interaction> interactions,
			TimelineType timelineType,
			Map<Residue,Tuple<String,String>> mutationMap,
			Map<Residue,String> secondaryStructureMap
			){
		return new AutoValue_InteractionImportData(
				ImmutableList.copyOf(interactions),
				timelineType,
				mutationMap,
				secondaryStructureMap);
	}
	
	public static InteractionImportData create(
			List<Interaction>  interactions,
			TimelineType timelineType){
		return create(
				interactions,
				timelineType,
				new SafeMap<>(),
				new SafeMap<>());
	}
	
	public abstract List<Interaction> getInteractions();
	public abstract TimelineType getTimelineType();
	public abstract Map<Residue,Tuple<String,String>> getMutationMap();
	public abstract Map<Residue,String> getSecondaryStructureMap();
	
	@Override
	public String reportParameters(){
		return "";
	}
	
}
