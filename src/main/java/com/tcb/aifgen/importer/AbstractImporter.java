package com.tcb.aifgen.importer;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.tcb.aifgen.util.ArrayUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.log.NotReported;
import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;


public abstract class AbstractImporter implements InteractionImporter {
	
	protected String normalizePattern(String pattern){
		if(pattern.trim().isEmpty()) throw new IllegalArgumentException("Atom mask must not be empty");
		else return pattern;
	}
	
	private Timeline takeEveryNth(Timeline t, Integer n){
		if(n < 1) throw new IllegalArgumentException("Sieve n must be >= 1");
		if(n.equals(1)) return t;
		double[] data = t.getData();
		int size = (int)Math.ceil(((float)data.length) / n);
		double[] result = new double[size];
		int j=0;
		for(int i=0;i<data.length;i+=n){
			result[j] = data[i];
			j++;
		}
		assert(j==result.length);
		return Timeline.create(result);
	}
		
	protected List<Interaction> filter(List<Interaction> interactions, Integer sieve, Double minAvg){
		List<Interaction> result = new ArrayList<>();
		for(Interaction in:interactions){
			Timeline t = in.getTimeline();
			t = takeEveryNth(t, sieve);
			double mean = ArrayUtil.mean(t.getData());
			if(mean < minAvg) continue;
			Interaction newIn = Interaction.create(
					in.getSourceAtom(),
					in.getTargetAtom(),
					in.getBridgingAtoms(),
					t,
					in.getType()
					);
			result.add(newIn);
		}
		return result;
	}
		
}
