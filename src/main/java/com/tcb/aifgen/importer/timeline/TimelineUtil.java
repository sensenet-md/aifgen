package com.tcb.aifgen.importer.timeline;

import java.util.Arrays;

import com.tcb.atoms.interactions.Timeline;

public class TimelineUtil {
	public static Boolean isEmpty(Timeline timeline){
		for(double d:timeline.getData()){
			if(d != 0.0) return false;
		}
		return true;
	}
}
