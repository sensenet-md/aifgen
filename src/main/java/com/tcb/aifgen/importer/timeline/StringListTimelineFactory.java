package com.tcb.aifgen.importer.timeline;

import java.util.ArrayList;
import java.util.List;

import com.tcb.atoms.interactions.Timeline;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TFloatArrayList;

public class StringListTimelineFactory {
	
	public StringListTimelineFactory(){
	}
	
		
	public Timeline create(String timeline){
		TDoubleArrayList result = new TDoubleArrayList();
		StringBuilder buffer = new StringBuilder();
		final int size = timeline.length();
		final int lastIndex = size - 1;
		for(int i=0;i<size;i++){
			char c = timeline.charAt(i);
			boolean isWhitespace = Character.isWhitespace(c);
			if(!isWhitespace){
				buffer.append(c);
			} 
			boolean isWhitespaceOrEnd = isWhitespace || i == lastIndex;
			if(buffer.length() > 0 && isWhitespaceOrEnd){
				result.add(Double.parseDouble((buffer.toString())));
				buffer.setLength(0);
			} 
		}
		
		return Timeline.create(result.toArray());
	}
	
}
