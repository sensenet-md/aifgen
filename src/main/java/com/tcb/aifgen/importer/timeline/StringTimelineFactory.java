package com.tcb.aifgen.importer.timeline;

import java.util.ArrayList;
import java.util.List;

import com.tcb.atoms.interactions.Timeline;

public class StringTimelineFactory {
	public static Timeline create(String timeline){
		List<Integer> result = new ArrayList<>();
		for(int i=0;i<timeline.length();i++){
			result.add(Integer.valueOf(String.valueOf(timeline.charAt(i))));
		}
		return Timeline.create(result);
	}
	
}
