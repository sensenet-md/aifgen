package com.tcb.aifgen.importer.timeline;

import java.util.ArrayList;
import java.util.List;

import com.tcb.atoms.interactions.Timeline;

public class DifferenceStringTimelineFactory {
	
	private static Integer parseChar(Character c){
		if(c.equals('2')) return 1;
		if(c.equals('3')) return -1;
		else return 0;
	}
	
	public static Timeline create(String timeline){
		List<Integer> result = new ArrayList<>();
		for(int i=0;i<timeline.length();i++){
			result.add(parseChar(timeline.charAt(i)));
		}
		return Timeline.create(result);
	}
	
}
