package com.tcb.aifgen.importer;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.log.ParameterReporter;

public interface InteractionImporter extends ParameterReporter {
	
	public static final String initializerError = "Importer fields not initialized";
	
	public InteractionImportData read() throws IOException;
	public String getName();
	
	
		
	/*@Override
	public default String reportParameters(){
		List<String> s = new ArrayList<>();
		s.add("Importer name: " + getName());
		s.add("Number of atoms: " + getInteractingAtoms().size());
		s.add("Number of residues: " + getInteractingResidues().size());
		s.add("Chains: " + getChains());
		s.add("Timeline type: " + getTimelineType());
		s.add("Timeline length: " + getTimelineLength());
		s.add("Interaction types: " + getInteractionTypes());
		return String.join("\n", s);
	}*/


}
