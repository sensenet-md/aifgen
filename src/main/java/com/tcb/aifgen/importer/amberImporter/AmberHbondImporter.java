package com.tcb.aifgen.importer.amberImporter;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.common.util.SafeMap;
import com.google.common.collect.ImmutableList;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class AmberHbondImporter extends AbstractImporter implements InteractionImporter {

	private static final int sourceFieldIndex = 2;
	private static final int targetFieldIndex = 0;
	private static final int hydrogenFieldIndex = 1;
	private static final int timeFractionIndex = 4;
	private static final String residueNameDelimiter = "_";
	private static final String atomNameDelimiter = "@";
	private static final TimelineType timelineType = TimelineType.TIMELINE;
	private static final String defaultChain = "";
	private static final String defaultInteractionType = InteractionType.HBOND.toString();

	private Path hbondsOutPath;
	private Path timelinePath;
	private String interactionType;
	private Set<String> ignoreAtomNames;
	private Integer sieve;
	private Double minAvg;
	
	public AmberHbondImporter(
			Path hbondsPath, Path timelinePath,
			String interactionType, Set<String> ignoreAtomNames,
			Integer sieve, Double minAvg) {
		this.hbondsOutPath = hbondsPath;
		this.timelinePath = timelinePath;
		this.interactionType = interactionType;
		this.ignoreAtomNames = ignoreAtomNames;
		this.sieve = sieve;
		this.minAvg = minAvg;
	}
	
	public AmberHbondImporter(Path hbondsPath, Path timelinePath){
		this(hbondsPath,timelinePath,defaultInteractionType,new HashSet<>(),1,Double.NEGATIVE_INFINITY);
	}
	
	public InteractionImportData read() throws IOException{
		return read(hbondsOutPath,timelinePath);
	}
			
	private InteractionImportData read(Path hbondsPath, Path timelinePath) throws IOException {
		AmberTimelinesReader amberTimelinesFile = new AmberTimelinesReader(timelinePath);
		amberTimelinesFile.init();
		
		Map<String,Timeline> timelines = amberTimelinesFile.getTimelines();
		CSV hbondsCsv = new CSV_Reader(hbondsPath.toString(), "\\s+", true, "").getCSV();
		
		List<Interaction> hbonds = new ArrayList<>();
		List<List<String>> lines = hbondsCsv.getRows();
		for(List<String> line : lines){
			String sourceField = line.get(sourceFieldIndex);
			String hydrogenField = line.get(hydrogenFieldIndex);
			String targetField = line.get(targetFieldIndex);
			Atom source = registerAtom(sourceField);
			Atom target = registerAtom(targetField);
			if(shouldIgnore(source) || shouldIgnore(target)) continue;
			Atom hydrogen = registerAtom(hydrogenField);
			Timeline timeline = getTimeline(timelines,source,target,hydrogen);
			Interaction interaction = Interaction.create(source,target,
					Arrays.asList(hydrogen),timeline, interactionType);
			hbonds.add(interaction);
		}
		hbonds = filter(hbonds, sieve, minAvg);
		
		return InteractionImportData.create(
				hbonds, getTimelineType());
	}
	
	private boolean shouldIgnore(Atom atom){
		return ignoreAtomNames.contains(atom.getName());
	}
	
	private Atom registerAtom(String field){
		//Integer atomIndex = Integer.valueOf((field.split(residueNameDelimiter)[2]));
		String atomName = field.split(atomNameDelimiter)[1].split(residueNameDelimiter)[0];
		//String element = null;
		Integer residueIndex = Integer.valueOf((field.split(residueNameDelimiter)[1]
				.split(atomNameDelimiter)[0]));
		String residueName = field.split(residueNameDelimiter)[0];
		String resInsert = "";
		String altLoc = "";
		Atom atom = Atom.create(atomName, 
				residueIndex, residueName, resInsert, altLoc, defaultChain
				);
		return atom;
	}
			
	private Timeline getTimeline(Map<String,Timeline> timelines, Atom source, Atom target, Atom hydrogen){
		String key = String.format("%s_%d@%s-%s_%d@%s-%s",
				target.getResidue().getName(),
				target.getResidue().getIndex(),
				target.getName(),
				source.getResidue().getName(),
				source.getResidue().getIndex(),
				source.getName(),
				hydrogen.getName());
		if(!timelines.containsKey(key))
			throw new RuntimeException("Could not find interaction: " + key);
		return timelines.get(key);
	}

	private TimelineType getTimelineType() {
		return timelineType;
	}

	@Override
	public String getName() {
		return getMainFilePath().getFileName().toString();
	}
	
	private Path getMainFilePath() {
		return hbondsOutPath;
	}
		
	public static String getDefaultInteractionType(){
		return defaultInteractionType;
	}
	
	
}
