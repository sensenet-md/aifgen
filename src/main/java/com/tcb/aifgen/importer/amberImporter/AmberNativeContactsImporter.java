package com.tcb.aifgen.importer.amberImporter;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.cytoscape.cyLib.util.MapUtil;
import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.google.common.collect.ImmutableList;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.aifgen.importer.pdbImporter.PdbAtom;
import com.tcb.aifgen.importer.pdbImporter.PdbReader;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class AmberNativeContactsImporter extends AbstractImporter implements InteractionImporter {

	private static final int contactFieldIndex = 1;
	private static final TimelineType timelineType = TimelineType.TIMELINE;
	private static final String defaultInteractionType = InteractionType.CONTACT.toString();
	
	private Path contactsPath;
	private List<Path> timelinePaths;
	private Path pdbPath;
	private Set<String> ignoredAtomNames;
	private Boolean ignoreIntraResidueContacts;
	private String interactionType;
	private Integer sieve;
	private Double minAvg;
	
	public AmberNativeContactsImporter(
			Path contactsPath, List<Path> timelinePaths, Path contactsPdbPath,
			String interactionType,
			Set<String> ignoreAtomNames,
			Boolean ignoreIntraResidueContacts,
			Integer sieve,
			Double minAvg) {
		this.contactsPath = contactsPath;
		this.timelinePaths = timelinePaths;
		this.pdbPath = contactsPdbPath;
		this.interactionType = interactionType;
		this.ignoredAtomNames = ignoreAtomNames;
		this.ignoreIntraResidueContacts = ignoreIntraResidueContacts;
		this.sieve = sieve;
		this.minAvg = minAvg;
	}
	
	public AmberNativeContactsImporter(
			Path contactsPath, List<Path> timelinePaths, Path contactsPdbPath){
		this(contactsPath,timelinePaths,contactsPdbPath,defaultInteractionType,new HashSet<>(),false,
				1,Double.NEGATIVE_INFINITY);
	}
	
	@Override
	public InteractionImportData read() throws IOException {
		List<List<String>> lines = parseLines(contactsPath);
		
		PdbReader pdbReader = new PdbReader(pdbPath);
		pdbReader.init();
		
		Map<String,Timeline> timelines = createTimelineMap();
		Map<Integer,Map<String,PdbAtom>> residueIndexAtomNamePdbAtomMap = 
				getResidueIndexAtomNamePdbAtomMap(pdbReader);
		
		List<Interaction> interactions = lines.stream()
				.map(line -> line.get(contactFieldIndex))
				.map(field -> getContact(field,residueIndexAtomNamePdbAtomMap,timelines))
				.collect(ImmutableList.toImmutableList());
		interactions = filterIgnoredAtoms(interactions);
		
		if(ignoreIntraResidueContacts) interactions = filterSameResidue(interactions);
		interactions = filter(interactions,sieve,minAvg);
		
		return InteractionImportData.create(interactions, getTimelineType());
	}
	
	private Map<String,Timeline> createTimelineMap() throws IOException {
		SafeMap<String,Timeline> result = new SafeMap<>();
		for(Path p:timelinePaths){
			AmberTimelinesReader timelinesReader = new AmberTimelinesReader(p);
			timelinesReader.init();
			Map<String,Timeline> timelines = timelinesReader.getTimelines();
			result.putAll(timelines);
		}
		return result;
	}
	
	private List<Interaction> filterIgnoredAtoms(List<Interaction> interactions){
		return interactions.stream()
				.filter(i -> !ignoredAtomNames.contains(i.getSourceAtom().getName()))
				.filter(i -> !ignoredAtomNames.contains(i.getTargetAtom().getName()))
				.collect(Collectors.toList());
	}
	
	private List<Interaction> filterSameResidue(List<Interaction> interactions){
		return interactions.stream()
				.filter(i -> !sameResidue(i))
				.collect(Collectors.toList());
	}
	
	private Boolean sameResidue(Interaction interaction){
		Residue a = interaction.getSourceAtom().getResidue();
		Residue b = interaction.getTargetAtom().getResidue();
		return a.equals(b);
	}
	
	

	private Interaction getContact(
			String field,
			Map<Integer,Map<String,PdbAtom>> residueIndexAtomNamePdbAtomMap,
			Map<String,Timeline> timelines){
		String sourceField = field.split("_")[0];
		String targetField = field.split("_")[1];
		Atom source = registerAtom(sourceField, residueIndexAtomNamePdbAtomMap);
		Atom target = registerAtom(targetField, residueIndexAtomNamePdbAtomMap);
		List<Atom> bridgingAtoms = new ArrayList<Atom>();
		String timelineKey = String.format(":%d@%s_:%d@%s", 
				source.getResidue().getIndex(),
				source.getName(),
				target.getResidue().getIndex(),
				target.getName());
		Timeline timeline = timelines.get(timelineKey);
		return Interaction.create(source,target,bridgingAtoms,timeline, interactionType);
	}
	
	private Atom registerAtom(String amberMask, Map<Integer,Map<String,PdbAtom>> residueIndexAtomNamePdbAtomMap){
		Integer resId = getResId(amberMask);
		String atomName = getAtomName(amberMask);
		if(!residueIndexAtomNamePdbAtomMap.containsKey(resId) ||
		   !residueIndexAtomNamePdbAtomMap.get(resId).containsKey(atomName)){
			throw new IllegalArgumentException(
					String.format("Could not find atom in pdb "
							+ "with residue index %d and atom name %s",resId,atomName));
		}
		PdbAtom pdbAtom = residueIndexAtomNamePdbAtomMap.get(resId).get(atomName);
									
		Residue pdbResidue = pdbAtom.getResidue();
		return Atom.create(
				 pdbAtom.getName(), 
				 pdbResidue.getIndex(), pdbResidue.getName(),
				 pdbResidue.getResidueInsert(), pdbResidue.getAltLoc(), 
				 pdbResidue.getChain());
	}
	
	private Map<Integer,Map<String,PdbAtom>> getResidueIndexAtomNamePdbAtomMap(PdbReader pdb){
		Collection<PdbAtom> pdbAtoms = pdb.getAtoms(0).values();
		return MapUtil.create2DMap(pdbAtoms, (p) -> p.getResidue().getIndex(), (p) -> p.getAtom().getName());
	}
	
	private Integer getResId(String amberMask){
		return Integer.valueOf(amberMask.split(":")[1].split("@")[0]);
	}
	
	private String getAtomName(String amberMask){
		return amberMask.split("@")[1];
	}
	
	private TimelineType getTimelineType() {
		return TimelineType.TIMELINE;
	}
	
	private List<List<String>> parseLines(Path path) throws IOException {
		BufferedReader lines;
		List<List<String>> result = new ArrayList<List<String>>();

		lines = Files.newBufferedReader(path);

		
		while(lines.ready()){
			String line = lines.readLine();
			if(line.startsWith("#")) continue;
			List<String> splitLine = Arrays.asList(line.trim().split("\\s+"));
			result.add(splitLine);
		}
		lines.close();
		
		return result;
	}

	@Override
	public String getName() {
		return getMainFilePath().getFileName().toString();
	}
	
	private Path getMainFilePath() {
		return contactsPath;
	}
	
	public static String getDefaultInteractionType(){
		return defaultInteractionType;
	}

		
}
