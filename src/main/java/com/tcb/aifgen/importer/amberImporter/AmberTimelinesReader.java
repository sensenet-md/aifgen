package com.tcb.aifgen.importer.amberImporter;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.tcb.atoms.interactions.Timeline;
import com.tcb.common.util.SafeMap;

public class AmberTimelinesReader {
	private Optional<Map<String,Timeline>> timelinesOpt;
	private Optional<Integer> timelineLengthOpt;
	private int timelineLength;
	private Path path;
	private static final RuntimeException notInitializedException = new RuntimeException("Object was not initialized");
	
	
	public AmberTimelinesReader(Path path){
		this.path = path;
		this.timelinesOpt = Optional.empty();
		this.timelineLengthOpt = Optional.empty();
	}
		
	public void init() throws IOException {
		this.timelinesOpt = Optional.of(parseTimelines(path));
		this.timelineLengthOpt = Optional.of(timelineLength);
	}
	
	private Map<String,Timeline> parseTimelines(Path timelinePath) throws IOException {
		Map<String,List<Integer>> timelines = new HashMap<>();
		BufferedReader lines = Files.newBufferedReader(timelinePath);
		String[] header = lines.readLine().trim().split("\\s+");
		for(int i=1;i<header.length;i++){
			String s = header[i];
			timelines.put(s, new ArrayList<>());
		}
		
		this.timelineLength = 0;
		while(lines.ready()){
			String[] line = lines.readLine().trim().split("\\s+");
			this.timelineLength++;
			for(int j=1;j<header.length;j++){
				String key = header[j];
				Integer newValue = Integer.valueOf(line[j]);
				timelines.get(key).add(newValue);
			}
		}
		lines.close();
		Map<String,Timeline> compressedTimelines = new SafeMap<String,Timeline>();
		timelines.forEach((k,v) -> compressedTimelines.put(k, Timeline.create(v)));
		return compressedTimelines;
	}
	
	public Map<String,Timeline> getTimelines(){
		return timelinesOpt.orElseThrow(() -> notInitializedException);
	}
	
	public Integer getTimelineLength(){
		return timelineLengthOpt.orElseThrow(() -> notInitializedException);
	}
}
