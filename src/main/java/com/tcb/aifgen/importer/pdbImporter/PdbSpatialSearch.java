package com.tcb.aifgen.importer.pdbImporter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tcb.aifgen.geometry.rtree.AtomSpatialSearch;
import com.tcb.aifgen.util.ListUtil;
import com.tcb.aifgen.util.MapUtil;
import com.tcb.common.util.SafeMap;

public class PdbSpatialSearch {

	public Map<PdbAtom,Set<PdbAtom>> getRangeMap(
			double distance,
			List<PdbAtom> sources,
			List<PdbAtom> targets) {
		Map<PdbAtom,Set<PdbAtom>> map = new SafeMap<>();
		AtomSpatialSearch search = AtomSpatialSearch.create(targets);
		for(PdbAtom source:sources){
			map.put(source, search.getInRange(source, distance));
		}
		return map;
	}
	
	public List<Map<PdbAtom,Set<PdbAtom>>> getRangeMaps(
			double distance,
			List<Integer> sourceListIndices,
			List<Integer> targetListIndices,
			List<Map<Integer,PdbAtom>> frames){
		List<Map<PdbAtom,Set<PdbAtom>>> rangeMaps = new ArrayList<>();
		
		for(Map<Integer,PdbAtom> frame:frames){
			List<PdbAtom> frameSources = MapUtil.getAll(frame, sourceListIndices);
			List<PdbAtom> frameTargets = MapUtil.getAll(frame, targetListIndices);
			Map<PdbAtom,Set<PdbAtom>> map = getRangeMap(distance, frameSources, frameTargets);
			rangeMaps.add(map);
		}
		return rangeMaps;
	}
}
