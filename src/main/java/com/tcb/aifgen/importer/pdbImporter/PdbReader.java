package com.tcb.aifgen.importer.pdbImporter;

import static com.tcb.aifgen.util.StringUtil.getTrimmedSubstring;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.tcb.common.exception.TooManyMatchesException;
import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.tcb.aifgen.util.StringUtil;
import com.tcb.atoms.residues.Residue;

public class PdbReader {
	private static final int LINE_LENGTH = 80;
	private static final List<String> recordTags = Arrays.asList("ATOM","HETATM");
	private static final String initializerError = "Reader fields not initialized";
	
	private List<Map<Integer,PdbAtom>> frames;
	private Optional<List<Map<Integer,PdbAtom>>> framesOpt;
	private int currentFrame = -1;
	private Path path;
	
	public PdbReader(Path path){
		this.path = path;
		reset();
	}
	
	public void init() throws IOException {
		BufferedReader reader = Files.newBufferedReader(path);
		while(reader.ready()){
			String line = prepareLine(reader.readLine());
			if(isNewModelLine(line)) selectNextFrame();
			if(!isAtomLine(line)) continue;
			if(currentFrame < 0) selectNextFrame();
			PdbAtom atom = createAtom(line);
			frames.get(currentFrame).put(atom.getAtomIndex(),atom);
			}
		reader.close();
		
		verifySameNumberOfAtomsInFrames(frames);
		
		framesOpt = Optional.of(frames);
	}
	
	public void reset(){
		frames = new ArrayList<>();
		framesOpt = Optional.empty();
	}
	
	private String prepareLine(String line){
		if(line.length() < LINE_LENGTH) 
			  line = StringUtil.append(line, " ", LINE_LENGTH - line.length());
		return line;
	}
	
	private void addNextFrameMap(List<Map<Integer,PdbAtom>> atoms){
		atoms.add(new SafeMap<>());
	}
	
	private void selectNextFrame(){
		addNextFrameMap(frames);
		currentFrame++;
	}
	
	private boolean isAtomLine(String line){
		String prefix = line.substring(0, 6).trim();
		if(recordTags.contains(prefix)) return true;
		else return false;
	}
	
	private boolean isNewModelLine(String line){
		if(line.startsWith("MODEL")) return true;
		else return false;
	}
	
	private PdbAtom createAtom(String line){
	  
	  // Get the required fields
	  int id = Integer.valueOf(getTrimmedSubstring(line,6,11));
	  String name = getTrimmedSubstring(line,12,16);
	  String alt_loc = String.valueOf(line.charAt(16)).trim();
	  String res_name = getTrimmedSubstring(line,17,20);
	  String chainName = String.valueOf(line.charAt(21)).trim();
	  int res_id = Integer.valueOf(getTrimmedSubstring(line,22,26));
	  String res_insert = String.valueOf(line.charAt(26)).trim();
	  double x = Double.valueOf(getTrimmedSubstring(line,30,38));
	  double y = Double.valueOf(getTrimmedSubstring(line,38,46));
	  double z = Double.valueOf(getTrimmedSubstring(line,46,54));

	  // these are optional
	  
	  /*double occupancy = Double.valueOf(getTrimmedSubstring(line,54,60));
	  double b = Double.valueOf(getTrimmedSubstring(line,60,66));
	  String segment = getTrimmedSubstring(line,72,76);
	  String element = getTrimmedSubstring(line,76,78);
	  double charge = Double.valueOf(getTrimmedSubstring(line,78,80));
	  */
	  String element = null;
	  
	  Residue residue = Residue.create(
			  res_id, res_name, res_insert, alt_loc, chainName);
	  
	  return PdbAtom.create(id, name, element, residue, x, y, z);
	}
	
	public Map<Integer,PdbAtom> getAtoms(Integer frame){
		return getFrames().get(frame);
	}
	
	public List<Map<Integer,PdbAtom>> getFrames(){
		return framesOpt.orElseThrow(() -> new RuntimeException(initializerError));
	}
	
	private void verifySameNumberOfAtomsInFrames(List<Map<Integer,PdbAtom>> frames){
		if(frames.size() == 0) throw new IllegalArgumentException("No atoms found");
		
		try{
			ListFilter.singleton(
					frames.stream()
					.map(l -> l.size())
					.collect(Collectors.toSet()))
					.orElseThrow(() -> new IllegalArgumentException("No atoms found"));
		} catch(TooManyMatchesException e){
			throw new IllegalArgumentException("Differing number of atoms in frames");
		}
			
	}	
}
