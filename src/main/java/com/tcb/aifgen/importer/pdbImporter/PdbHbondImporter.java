package com.tcb.aifgen.importer.pdbImporter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.ml.neuralnet.MapUtils;

import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.tcb.aifgen.geometry.rtree.AtomSpatialSearch;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.aifgen.importer.timeline.TimelineUtil;
import com.tcb.aifgen.util.ListUtil;
import com.tcb.aifgen.util.MapUtil;
import com.tcb.aifgen.util.MathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.log.NotReported;
import com.tcb.monitor.Monitor;
import com.tcb.monitor.MonitorImpl;

public class PdbHbondImporter extends AbstractImporter {

	private Path pdbPath;
	private Double distanceCutoff;
	private Double angleCutoff;
	private String donorPattern;
	private String acceptorPattern;
	private String interactionType;
	private Set<String> ignoreAtomNames;
	
	private static final String defaultInteractionType = InteractionType.HBOND.toString();
	private static final double donorHydrogenDistanceCutoff = 1.5;
	
	public PdbHbondImporter(
			Path path, Double distanceCutoff, Double angleCutoff,
			String donorPattern, String acceptorPattern, String interactionType,
			Set<String> ignoreAtomNames){
		this.pdbPath = path;
		this.distanceCutoff = distanceCutoff;
		this.angleCutoff = angleCutoff;
		this.donorPattern = normalizePattern(donorPattern);
		this.acceptorPattern = normalizePattern(acceptorPattern);
		this.interactionType = interactionType;
		this.ignoreAtomNames = ignoreAtomNames;
	}
	
	public PdbHbondImporter(Path path, Double distanceCutoff, Double angleCutoff,
			String donorPattern, String acceptorPattern){
		this(path,distanceCutoff,angleCutoff,donorPattern,acceptorPattern,defaultInteractionType,new HashSet<>());
	}
				
	@Override
	public InteractionImportData read() throws IOException {
		PdbReader pdb = new PdbReader(pdbPath);
		pdb.init();
		List<Map<Integer,PdbAtom>> frames = pdb.getFrames();

		List<Interaction> interactions = getInteractions(frames);
		
		return InteractionImportData.create(interactions, getTimelineType());		
	}

	private TimelineType getTimelineType() {
		return TimelineType.TIMELINE;
	}
	
	private List<PdbAtom> filterDonors(Collection<PdbAtom> atoms){
		return atoms.stream()
				.filter(a -> a.getName().matches(donorPattern))
				.filter(a -> !ignoreAtomNames.contains(a.getName()))
				.filter(a -> !a.getName().startsWith("H"))
				.collect(Collectors.toList());
	}
	
	private List<PdbAtom> filterAcceptors(Collection<PdbAtom> atoms){
		return atoms.stream()
				.filter(a -> a.getName().matches(acceptorPattern))
				.filter(a -> !ignoreAtomNames.contains(a.getName()))
				.filter(a -> !a.getName().startsWith("H"))
				.collect(Collectors.toList());
	}
	
	private List<PdbAtom> filterHydrogens(Collection<PdbAtom> atoms){
		return atoms.stream()
				.filter(a -> a.getName().startsWith("H"))
				.collect(Collectors.toList());
	}
		
	private double angle(PdbAtom a1, PdbAtom a2, PdbAtom a3){
		return MathUtil.anglePointsDeg(a1.getVector(), a2.getVector(), a3.getVector());
	}
	
	private List<Interaction> getInteractions(List<Map<Integer,PdbAtom>> frames){
		ImmutableList.Builder<Interaction> result = ImmutableList.builder();
		
		Map<Atom,Map<Atom,Map<Atom,List<Integer>>>> dahMap = getDonorAcceptorHydrogenTimelineMap(frames);
		
		for(Atom donor:dahMap.keySet()){
			Map<Atom,Map<Atom,List<Integer>>> map1 = dahMap.get(donor);
			for(Atom acceptor:map1.keySet()){
				Map<Atom,List<Integer>> map2 = map1.get(acceptor);
				for(Atom hydrogen:map2.keySet()){
					List<Integer> activeFrames = map2.get(hydrogen);
					int[] timelineData = new int[frames.size()];
					for(Integer activeFrame:activeFrames){
						timelineData[activeFrame] = 1;
					}
					Timeline timeline = Timeline.create(timelineData);
					Interaction interaction = Interaction.create(
							donor,
							acceptor,
							Arrays.asList(hydrogen),
							timeline,
							interactionType);
					result.add(interaction);
				}
			}
		}
				
		return result.build();
	}
	
	private Map<Atom,Map<Atom,Map<Atom,List<Integer>>>> getDonorAcceptorHydrogenTimelineMap(
			List<Map<Integer,PdbAtom>> frames
			){
		Map<Integer,PdbAtom> frame0Atoms = frames.get(0);
		
		List<Integer> donorListIndices = getDonorListIndices(frame0Atoms.values());
		List<Integer> acceptorListIndices = getAcceptorListIndices(frame0Atoms.values());
		List<Integer> hydrogenListIndices = getHydrogenListIndices(frame0Atoms.values());
		
		PdbSpatialSearch search = new PdbSpatialSearch();
		
		List<Map<PdbAtom,Set<PdbAtom>>> donorAcceptorRangeMaps = 
				search.getRangeMaps(distanceCutoff, donorListIndices, acceptorListIndices, frames);
				
		List<Map<PdbAtom,Set<PdbAtom>>> donorHydrogenRangeMaps = 
				search.getRangeMaps(donorHydrogenDistanceCutoff, donorListIndices, hydrogenListIndices, frames);
				
		Map<Atom,Map<Atom,Map<Atom,List<Integer>>>> dahMap = new SafeMap<>();
		for(int i=0;i<frames.size();i++){
			Map<Integer,PdbAtom> frame = frames.get(i);
			Map<PdbAtom,Set<PdbAtom>> donorAcceptorRangeMap = donorAcceptorRangeMaps.get(i);
			Map<PdbAtom,Set<PdbAtom>> donorHydrogenRangeMap = donorHydrogenRangeMaps.get(i);
			for(Integer donorIndex:donorListIndices){
				PdbAtom donor = frame.get(donorIndex);
				Set<PdbAtom> acceptors = donorAcceptorRangeMap.get(donor);
				for(PdbAtom acceptor:acceptors){
					Set<PdbAtom> hydrogens = donorHydrogenRangeMap.get(donor).stream()
							.filter(a -> a.getResidue().equals(donor.getResidue()))
							.collect(ImmutableSet.toImmutableSet());
					for(PdbAtom hydrogen:hydrogens){
						if(angle(donor,hydrogen,acceptor) < angleCutoff) continue;
						Atom donorA = donor.getAtom();
						Atom acceptorA = acceptor.getAtom();
						Atom hydrogenA = hydrogen.getAtom();
						
						Map<Atom,Map<Atom,List<Integer>>> map1 = 
								MapUtil.getOrPut(dahMap, donorA, SafeMap::new);
							Map<Atom,List<Integer>> map2 = 
									MapUtil.getOrPut(map1, acceptorA, SafeMap::new);
						List<Integer> active = 
								MapUtil.getOrPut(map2, hydrogenA, ArrayList::new);
						active.add(i);
					}
				}
			}
		}
		return dahMap;
	}
	
	private List<Integer> getDonorListIndices(Collection<PdbAtom> atoms){
		List<PdbAtom> donors = filterDonors(atoms);
		if(donors.isEmpty()){
			throw new IllegalArgumentException("Found no donors in pdb");
		}
		return donors.stream()
				.map(a -> a.getAtomIndex())
				.collect(ImmutableList.toImmutableList());
	}
	
	private List<Integer> getAcceptorListIndices(Collection<PdbAtom> atoms){
		List<PdbAtom> acceptors = filterAcceptors(atoms);
		if(acceptors.isEmpty()){
			throw new IllegalArgumentException("Found no acceptors in pdb");
		}
		return acceptors.stream()
				.map(a -> a.getAtomIndex())
				.collect(ImmutableList.toImmutableList());
	}
	
	private List<Integer> getHydrogenListIndices(Collection<PdbAtom> atoms){
		List<PdbAtom> hydrogens = filterHydrogens(atoms);
		if(hydrogens.isEmpty()) {
			throw new IllegalArgumentException("Found no hydrogens in pdb");
		}
		return hydrogens.stream()
				.map(a -> a.getAtomIndex())
				.collect(ImmutableList.toImmutableList());
	}
					
	@Override
	public String getName() {
		return getMainFilePath().getFileName().toString();
	}
	
	private Path getMainFilePath() {
		return pdbPath;
	}
	
	public static String getDefaultInteractionType(){
		return defaultInteractionType;
	}
			
}
