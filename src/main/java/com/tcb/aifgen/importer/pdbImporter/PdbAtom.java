package com.tcb.aifgen.importer.pdbImporter;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.google.auto.value.AutoValue;

import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.residues.Residue;


@AutoValue
public abstract class PdbAtom {
	
	public static PdbAtom create(Integer atomIndex,
			Atom atom, Double x, Double y, Double z) {
		return new AutoValue_PdbAtom(atomIndex,atom,x,y,z);
	}
	
	public static PdbAtom create (
			Integer atomIndex, String name, String element,
			Residue residue, Double x, Double y, Double z){
		Atom atom = Atom.create(name, residue);
		return create(atomIndex, atom,x,y,z);
	}
	
	public static PdbAtom create(
			Integer atomIndex, String name, String element,
			Integer residueIndex, String residueName, String residueInsert,
			String altLoc, String chain,
			Double x, Double y, Double z) {
		Residue residue = Residue.create(residueIndex, residueName, residueInsert, altLoc, chain);
		return create(atomIndex,name,element,residue,x,y,z);
	}
		

	public abstract Integer getAtomIndex();
	
	public abstract Atom getAtom();
	public abstract double getX();
	public abstract double getY();
	public abstract double getZ();

	public Vector3D getVector() {
		return new Vector3D(getX(),getY(),getZ());
	}
		
	public String getName(){
		return getAtom().getName();
	};
	
	public Residue getResidue(){
		return getAtom().getResidue();
	}
	
	

}
