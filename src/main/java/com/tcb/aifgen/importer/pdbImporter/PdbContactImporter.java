package com.tcb.aifgen.importer.pdbImporter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.tcb.common.util.SafeMap;
import com.tcb.common.util.Tuple;
import com.google.common.collect.ImmutableList;
import com.tcb.aifgen.importer.AbstractImporter;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.TimelineType;
import com.tcb.aifgen.importer.timeline.TimelineUtil;
import com.tcb.aifgen.util.MapUtil;
import com.tcb.aifgen.util.MathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;
import com.tcb.cytoscape.cyLib.log.NotReported;

public class PdbContactImporter extends AbstractImporter {
	
	private Path pdbPath;
	private Double distanceCutoff;
	private String atomPattern;
	private Set<String> ignoredAtomNames;
	private Boolean ignoreIntraResidueContacts;
	private String interactionType;

	private static final String defaultInteractionType = InteractionType.CONTACT.toString();
	
	public PdbContactImporter(
			Path path,
			Double distanceCutoff,
			String atomPattern){
		this(path,distanceCutoff,atomPattern,defaultInteractionType,new HashSet<>(),false);
	}
	
	public PdbContactImporter(
			Path path,
			Double distanceCutoff,
			String atomPattern,
			String interactionType,
			Set<String> ignoreAtomNames,
			Boolean ignoreIntraResidueContacts){
		this.pdbPath = path;
		this.distanceCutoff = distanceCutoff;
		this.atomPattern = normalizePattern(atomPattern);
		this.ignoredAtomNames = ignoreAtomNames;
		this.ignoreIntraResidueContacts = ignoreIntraResidueContacts;
		this.interactionType = interactionType;
	}
	
	
	@Override
	public InteractionImportData read() throws IOException {
		PdbReader pdb = new PdbReader(pdbPath);
		pdb.init();
		List<Map<Integer,PdbAtom>> frames = pdb.getFrames();
								
		List<Interaction> interactions = createInteractionsInCutoffDistance(frames);
		
		return InteractionImportData.create(interactions, getTimelineType());		
	}

	private TimelineType getTimelineType() {
		return TimelineType.TIMELINE;
	}
	
	private List<PdbAtom> filterAtomPattern(Collection<PdbAtom> atoms){
		return atoms.stream()
				.filter(a -> a.getName().matches(atomPattern))
				.filter(a -> !a.getName().startsWith("H"))
				.filter(a -> !ignoredAtomNames.contains(a.getName()))
				.collect(Collectors.toList());
	}
			
	private List<Interaction> createInteractionsInCutoffDistance(List<Map<Integer,PdbAtom>> frames) {
		ImmutableList.Builder<Interaction> interactions = ImmutableList.builder();
		
		Map<Atom,Map<Atom,List<Integer>>> contactTimelineMap = createContactTimelineMap(frames);
		
		for(Atom donor:contactTimelineMap.keySet()){
			Map<Atom,List<Integer>> map = contactTimelineMap.get(donor);
			for(Atom acceptor:map.keySet()){
				List<Integer> activeFrames = map.get(acceptor);
				int[] timelineData = new int[frames.size()];
				for(Integer activeFrame:activeFrames){
					timelineData[activeFrame] = 1;
				}
				Timeline timeline = Timeline.create(timelineData);
				Interaction interaction = createInteraction(donor,acceptor,timeline);
				interactions.add(interaction);
			}
		}		
		return interactions.build();
	}
	
	private Map<Atom,Map<Atom,List<Integer>>> createContactTimelineMap(List<Map<Integer,PdbAtom>> frames){
		List<Integer> atomIndices = filterAtomPattern(frames.get(0).values()).stream()
				.map(a -> a.getAtomIndex())
				.collect(ImmutableList.toImmutableList());
				
		PdbSpatialSearch search = new PdbSpatialSearch();
		List<Map<PdbAtom,Set<PdbAtom>>> contactRangeMaps = search.getRangeMaps(
				distanceCutoff,atomIndices,atomIndices, frames);
		
		Map<Atom,Map<Atom,List<Integer>>> result = new SafeMap<>();
		for(int i=0;i<frames.size();i++){
			Map<PdbAtom,Set<PdbAtom>> rangeMap = contactRangeMaps.get(i);
			for(PdbAtom donor:rangeMap.keySet()){
				Set<PdbAtom> acceptors = rangeMap.get(donor);
				for(PdbAtom acceptor:acceptors){
					if(donor.getAtomIndex() >= acceptor.getAtomIndex()) continue;
					if(ignoreIntraResidueContacts && isSameResidue(donor,acceptor)) continue;
					Atom donorA = donor.getAtom();
					Atom acceptorA = acceptor.getAtom();
					Map<Atom,List<Integer>> map = MapUtil.getOrPut(result,donorA,SafeMap::new);
					List<Integer> activeFrames = MapUtil.getOrPut(map,acceptorA,ArrayList::new);
					activeFrames.add(i);
				}
			}
		}
		return result;
	}
		
	private Boolean isSameResidue(PdbAtom a1, PdbAtom a2){
		return a1.getResidue().equals(a2.getResidue());
	}
	
	private Interaction createInteraction(Atom a1, Atom a2, Timeline timeline){
		List<Atom> bridgingAtoms = new ArrayList<Atom>();
		Interaction contact = Interaction.create(
				a1,a2,bridgingAtoms, timeline,
				interactionType);
		return contact;
	}

	@Override
	public String getName() {
		return getMainFilePath().getFileName().toString();
	}
	
	private Path getMainFilePath() {
		return pdbPath;
	}
	
	public static String getDefaultInteractionType(){
		return defaultInteractionType;
	}
	
	private Integer getListIndex(PdbAtom a){
		return a.getAtomIndex() - 1;
	}
				
}
