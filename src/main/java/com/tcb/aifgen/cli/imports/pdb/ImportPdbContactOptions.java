package com.tcb.aifgen.cli.imports.pdb;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.tcb.aifgen.cli.CommonOptions;

public class ImportPdbContactOptions extends CommonOptions {
	
	private static final String defaultAtomPattern = "C.*";
	
	public ImportPdbContactOptions(){
		super();

		this.addOption(
				Option.builder(null)
				.longOpt("pdb")
				.argName("FILE")
				.type(String.class)
				.hasArg()
				.desc("input .pdb file")
				.required()
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("distance")
				.argName("FLOAT")
				.type(String.class)
				.hasArg()
				.desc("input distance cutoff")
				.required()
				.build()
				);
			
		this.addOption(
				Option.builder(null)
				.longOpt("atomPattern")
				.argName("REGEX")
				.type(String.class)
				.hasArg()
				.desc("(opt.) Java regex pattern for contacting atoms. Defaults to: " + defaultAtomPattern)
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("ignoreIntraResidue")
				.type(Boolean.class)
				.desc("(opt.) ignore intra residue contacts (default no)")
				.build()
				);
						
		this.addOption(
				Option.builder(null)
				.longOpt("ignoreAtomNames")
				.argName("STRING")
				.type(String.class)
				.hasArg()
				.desc("(opt.) comma separated atom names to ignore ")
				.build()
				);

	}
	
	public Path getPdbPath(CommandLine commandLine){
		return Paths.get(commandLine.getOptionValue("pdb"));
	}
	
	public Double getDistanceCutoff(CommandLine commandLine){
		return Double.parseDouble(commandLine.getOptionValue("distance"));
	}
		
	public String getAtomPattern(CommandLine commandLine){
		return commandLine.getOptionValue("donorPattern", defaultAtomPattern);
	}
		
	public Set<String> getIgnoredAtomNames(CommandLine commandLine){
		String v = commandLine.getOptionValue("ignoreAtomNames");
		if(v==null) return new HashSet<>();
		return Stream.of(v.split(",")).collect(Collectors.toSet());
	}
	
	public Boolean getShouldIgnoreIntraResidue(CommandLine commandLine){
		return commandLine.hasOption("ignoreIntraResidue");
	}
	
}
