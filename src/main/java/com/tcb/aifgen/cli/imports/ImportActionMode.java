package com.tcb.aifgen.cli.imports;

import java.util.List;

import com.tcb.aifgen.cli.imports.aif.ImportAifAction;
import com.tcb.aifgen.cli.imports.aif.ImportZaifAction;
import com.tcb.aifgen.cli.imports.cpptraj.ImportCpptrajContactsAction;
import com.tcb.aifgen.cli.imports.cpptraj.ImportCpptrajHbondsAction;
import com.tcb.aifgen.cli.imports.pdb.ImportPdbContactAction;
import com.tcb.aifgen.cli.imports.pdb.ImportPdbHbondsAction;

public enum ImportActionMode {
	IMPORT_AIF,IMPORT_ZAIF,
	IMPORT_CPPTRAJ_HBONDS,IMPORT_CPPTRAJ_CONTACTS,
	IMPORT_PDB_HBONDS, IMPORT_PDB_CONTACTS;

	
	public ImportAction getAction(List<String> arguments){
		switch(this){
		case IMPORT_AIF: return ImportAifAction.create(arguments);
		case IMPORT_CPPTRAJ_CONTACTS: return ImportCpptrajContactsAction.create(arguments);
		case IMPORT_CPPTRAJ_HBONDS: return ImportCpptrajHbondsAction.create(arguments);
		case IMPORT_ZAIF: return ImportZaifAction.create(arguments);
		case IMPORT_PDB_CONTACTS: return ImportPdbContactAction.create(arguments);
		case IMPORT_PDB_HBONDS: return ImportPdbHbondsAction.create(arguments);
		default: throw new UnsupportedOperationException();
		}
	}
}
