package com.tcb.aifgen.cli.imports.pdb;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;

import com.tcb.aifgen.cli.ArgumentParser;
import com.tcb.aifgen.cli.imports.ImportAction;
import com.tcb.aifgen.cli.imports.aif.ImportAifOptions;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbHbondImporter;

public class ImportPdbHbondsAction implements ImportAction {
	
	private CommandLine cmd;
	private ImportPdbHbondOptions options;
	
	private ImportPdbHbondsAction(CommandLine cmd, ImportPdbHbondOptions options){
		this.cmd = cmd;
		this.options = options;
	}
	
	public static ImportPdbHbondsAction create(List<String> arguments){
		ImportPdbHbondOptions options = new ImportPdbHbondOptions();
		CommandLine cmd = ArgumentParser.parseArguments(options, arguments);
		return new ImportPdbHbondsAction(cmd, options);
	}
	
	@Override
	public InteractionImporter read() {
		Path pdbPath = options.getPdbPath(cmd);
		Double distanceCutoff = options.getDistanceCutoff(cmd);
		Double angleCutoff = options.getAngleCutoff(cmd);
		String donorPattern = options.getDonorPattern(cmd);
		String acceptorPattern = options.getAcceptorPattern(cmd);
		Set<String> atomNamesToIgnore = options.getIgnoredAtomNames(cmd);
		return new PdbHbondImporter(pdbPath, distanceCutoff, angleCutoff, donorPattern, acceptorPattern,
				PdbHbondImporter.getDefaultInteractionType(),
				atomNamesToIgnore
				);
	}

}
