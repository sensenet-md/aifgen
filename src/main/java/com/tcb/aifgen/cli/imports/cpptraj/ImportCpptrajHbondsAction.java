package com.tcb.aifgen.cli.imports.cpptraj;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;

import com.tcb.aifgen.cli.ArgumentParser;
import com.tcb.aifgen.cli.imports.ImportAction;
import com.tcb.aifgen.cli.imports.aif.ImportAifOptions;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;

public class ImportCpptrajHbondsAction implements ImportAction {
	
	private CommandLine cmd;
	private ImportCpptrajHbondOptions options;
	
	private ImportCpptrajHbondsAction(CommandLine cmd, ImportCpptrajHbondOptions options){
		this.cmd = cmd;
		this.options = options;
	}
	
	public static ImportCpptrajHbondsAction create(List<String> arguments){
		ImportCpptrajHbondOptions options = new ImportCpptrajHbondOptions();
		CommandLine cmd = ArgumentParser.parseArguments(options, arguments);
		return new ImportCpptrajHbondsAction(cmd, options);
	}
	
	@Override
	public InteractionImporter read() {
		Path outFilePath = options.getOutfilePath(cmd);
		Path seriesPath = options.getSeriesPath(cmd);
		Set<String> atomNamesToIgnore = options.getIgnoredAtomNames(cmd);
		Integer sieve = options.getSieve(cmd);
		Double minAvg = options.getMinAvg(cmd);
		return new AmberHbondImporter(
				outFilePath,
				seriesPath,
				AmberHbondImporter.getDefaultInteractionType(),
				atomNamesToIgnore, sieve, minAvg);
	}

}
