package com.tcb.aifgen.cli.imports.cpptraj;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.tcb.aifgen.cli.CommonOptions;

public class ImportCpptrajContactsOptions extends CommonOptions {
	
	public ImportCpptrajContactsOptions(){
		super();

		this.addOption(
				Option.builder(null)
				.longOpt("out")
				.argName("FILE")
				.type(String.class)
				.hasArg()
				.desc("input cpptraj nativecontacts .out file")
				.required()
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("nativeSeries")
				.argName("FILE")
				.type(String.class)
				.hasArg()
				.desc("input cpptraj nativecontacts .series file")
				.required()
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("nonNativeSeries")
				.argName("FILE")
				.type(String.class)
				.hasArg()
				.desc("(opt.) input cpptraj nativecontacts nonnative .series file")
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("pdb")
				.argName("FILE")
				.type(String.class)
				.hasArg()
				.desc("(opt.) input cpptraj nativecontacts .pdb file")
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("ignoreAtomNames")
				.argName("STRING")
				.type(String.class)
				.hasArg()
				.desc("(opt.) comma separated atom names to ignore ")
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("ignoreIntraResidue")
				.type(Boolean.class)
				.desc("(opt.) ignore intra residue contacts (default no)")
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("sieve")
				.type(String.class)
				.argName("INTEGER")
				.hasArg()
				.desc("(opt.) use every nth frame (default 1)")
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("minAvg")
				.argName("FLOAT")
				.type(String.class)
				.hasArg()
				.desc("(opt.) minimum average for timeline (default -Inf)")
				.build()
				);

	}
	
	public Path getOutfilePath(CommandLine commandLine){
		return Paths.get(commandLine.getOptionValue("out"));
	}
	
	public Optional<Path> getNonNativeSeriesPath(CommandLine commandLine){
		Optional<String> arg = Optional.ofNullable(commandLine.getOptionValue("nonNativeSeries"));
		if(!arg.isPresent()) return Optional.empty();
		return Optional.of(Paths.get(arg.get()));
	}
	
	public Path getNativeSeriesPath(CommandLine commandLine){
		return Paths.get(commandLine.getOptionValue("nativeSeries"));
	}
	
	public Path getPdbPath(CommandLine cmd){
		return Paths.get(cmd.getOptionValue("pdb"));
	}
	
	public Set<String> getIgnoredAtomNames(CommandLine commandLine){
		String v = commandLine.getOptionValue("ignoreAtomNames");
		if(v==null) return new HashSet<>();
		return Stream.of(v.split(",")).collect(Collectors.toSet());
	}
	
	public Boolean getShouldIgnoreIntraResidue(CommandLine commandLine){
		return commandLine.hasOption("ignoreIntraResidue");
	}
	
	public Integer getSieve(CommandLine commandLine){
		String sieve = commandLine.getOptionValue("sieve");
		if(sieve==null) return 1;
		return Integer.valueOf(sieve);
	}
	
	public Double getMinAvg(CommandLine commandLine){
		String minAvg = commandLine.getOptionValue("minAvg");
		if(minAvg==null) return Double.NEGATIVE_INFINITY;
		return Double.valueOf(minAvg);
	}
	

	
}
