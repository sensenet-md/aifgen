package com.tcb.aifgen.cli.imports.pdb;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;

import com.tcb.aifgen.cli.ArgumentParser;
import com.tcb.aifgen.cli.imports.ImportAction;
import com.tcb.aifgen.cli.imports.aif.ImportAifOptions;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbContactImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbHbondImporter;

public class ImportPdbContactAction implements ImportAction {
	
	private CommandLine cmd;
	private ImportPdbContactOptions options;
	
	private ImportPdbContactAction(CommandLine cmd, ImportPdbContactOptions options){
		this.cmd = cmd;
		this.options = options;
	}
	
	public static ImportPdbContactAction create(List<String> arguments){
		ImportPdbContactOptions options = new ImportPdbContactOptions();
		CommandLine cmd = ArgumentParser.parseArguments(options, arguments);
		return new ImportPdbContactAction(cmd, options);
	}
	
	@Override
	public InteractionImporter read() {
		Path pdbPath = options.getPdbPath(cmd);
		Double distanceCutoff = options.getDistanceCutoff(cmd);
		String atomPattern = options.getAtomPattern(cmd);
		Set<String> atomNamesToIgnore = options.getIgnoredAtomNames(cmd);
		Boolean ignoreIntraResidue = options.getShouldIgnoreIntraResidue(cmd);
		return new PdbContactImporter(pdbPath, distanceCutoff, atomPattern,
				PdbContactImporter.getDefaultInteractionType(),
				atomNamesToIgnore, ignoreIntraResidue
				);
	}

}
