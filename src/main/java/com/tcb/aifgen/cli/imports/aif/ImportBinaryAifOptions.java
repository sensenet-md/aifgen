package com.tcb.aifgen.cli.imports.aif;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.tcb.aifgen.cli.CommonOptions;

public class ImportBinaryAifOptions extends CommonOptions {
	
	public ImportBinaryAifOptions(){
		super();

		this.addOption(
				Option.builder("i")
				.argName("FILE")
				.type(String.class)
				.hasArg()
				.desc("input .baif file")
				.required()
				.build()
				);		
	}
	
	public Path getBaifPath(CommandLine commandLine){
		return Paths.get(commandLine.getOptionValue("i"));
	}
		
}
