package com.tcb.aifgen.cli.imports;



import java.util.List;

import org.apache.commons.cli.ParseException;

import com.tcb.aifgen.importer.InteractionImporter;

public interface ImportAction {
	public InteractionImporter read();
}
