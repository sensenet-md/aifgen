package com.tcb.aifgen.cli.imports.aif;

import java.nio.file.Path;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;

import com.tcb.aifgen.cli.ArgumentParser;
import com.tcb.aifgen.cli.imports.ImportAction;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifImporter;
import com.tcb.aifgen.importer.aifImporter.ZaifImporter;

public class ImportZaifAction implements ImportAction {

	private CommandLine cmd;
	private ImportAifOptions options;

	public static ImportZaifAction create(List<String> arguments){
		ImportAifOptions options = new ImportAifOptions();
		CommandLine cmd = ArgumentParser.parseArguments(options, arguments);
		return new ImportZaifAction(cmd,options);
	}
	
	private ImportZaifAction(CommandLine cmd,ImportAifOptions options){
		this.cmd = cmd;
		this.options = options;
	}
	
	@Override
	public InteractionImporter read() {
		Path p = options.getAifPath(cmd);
		Integer sieve = options.getSieve(cmd);
		Double minAvg = options.getMinAvg(cmd);
		return new ZaifImporter(p,sieve,minAvg);
	}

}
