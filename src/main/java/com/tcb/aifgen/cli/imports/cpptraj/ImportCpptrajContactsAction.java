package com.tcb.aifgen.cli.imports.cpptraj;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;

import com.tcb.aifgen.cli.ArgumentParser;
import com.tcb.aifgen.cli.imports.ImportAction;
import com.tcb.aifgen.cli.imports.aif.ImportAifAction;
import com.tcb.aifgen.cli.imports.aif.ImportAifOptions;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.amberImporter.AmberNativeContactsImporter;

public class ImportCpptrajContactsAction implements ImportAction {
	
	private CommandLine cmd;
	private ImportCpptrajContactsOptions options;

	
	public ImportCpptrajContactsAction(CommandLine cmd, ImportCpptrajContactsOptions options){
		this.cmd = cmd;
		this.options = options;
	}
	
	public static ImportCpptrajContactsAction create(List<String> arguments){
		ImportCpptrajContactsOptions options = new ImportCpptrajContactsOptions();
		CommandLine cmd = ArgumentParser.parseArguments(options, arguments);
		return new ImportCpptrajContactsAction(cmd, options);
	}
		
	@Override
	public InteractionImporter read() {
		Path outFilePath = options.getOutfilePath(cmd);
		Path nativeSeriesPath = options.getNativeSeriesPath(cmd);
		Optional<Path> nonNativeSeriesPath = options.getNonNativeSeriesPath(cmd);
		Path pdbPath = options.getPdbPath(cmd);
		Set<String> atomNamesToIgnore = options.getIgnoredAtomNames(cmd);
		
		List<Path> seriesPaths = new ArrayList<>();
		seriesPaths.add(nativeSeriesPath);
		if(nonNativeSeriesPath.isPresent()){
			seriesPaths.add(nonNativeSeriesPath.get());
		}
		
		Boolean shouldIgnoreIntraResidue = options.getShouldIgnoreIntraResidue(cmd);
		Integer sieve = options.getSieve(cmd);
		Double minAvg = options.getMinAvg(cmd);
		
		return new AmberNativeContactsImporter(
				outFilePath,
				seriesPaths,
				pdbPath,
				AmberNativeContactsImporter.getDefaultInteractionType(),
				atomNamesToIgnore, shouldIgnoreIntraResidue,
				sieve, minAvg);
	}

}
