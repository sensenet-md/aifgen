package com.tcb.aifgen.cli.imports.pdb;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.tcb.aifgen.cli.CommonOptions;

public class ImportPdbHbondOptions extends CommonOptions {
	
	private static final String defaultDonorPattern = "F.*|O.*|N.*";
	private static final String defaultAcceptorPattern = "F.*|O.*|N.*";
	
	public ImportPdbHbondOptions(){
		super();

		this.addOption(
				Option.builder(null)
				.longOpt("pdb")
				.argName("FILE")
				.type(String.class)
				.hasArg()
				.desc("input .pdb file")
				.required()
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("distance")
				.argName("FLOAT")
				.type(String.class)
				.hasArg()
				.desc("input distance cutoff")
				.required()
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("angle")
				.argName("FLOAT")
				.type(String.class)
				.hasArg()
				.desc("input angle cutoff")
				.required()
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("donorPattern")
				.argName("REGEX")
				.type(String.class)
				.hasArg()
				.desc("(opt.) Java regex pattern for donor atoms. Defaults to: " + defaultDonorPattern)
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("acceptorPattern")
				.argName("REGEX")
				.type(String.class)
				.hasArg()
				.desc("(opt.) Java regex pattern for acceptor atoms. Defaults to: " + defaultAcceptorPattern)
				.build()
				);
				
		this.addOption(
				Option.builder(null)
				.longOpt("ignoreAtomNames")
				.argName("STRING")
				.type(String.class)
				.hasArg()
				.desc("(opt.) comma separated atom names to ignore ")
				.build()
				);

	}
	
	public Path getPdbPath(CommandLine commandLine){
		return Paths.get(commandLine.getOptionValue("pdb"));
	}
	
	public Double getDistanceCutoff(CommandLine commandLine){
		return Double.parseDouble(commandLine.getOptionValue("distance"));
	}
	
	public Double getAngleCutoff(CommandLine commandLine){
		return Double.parseDouble(commandLine.getOptionValue("angle"));
	}
	
	public String getDonorPattern(CommandLine commandLine){
		return commandLine.getOptionValue("donorPattern", defaultDonorPattern);
	}
	
	public String getAcceptorPattern(CommandLine commandLine){
		return commandLine.getOptionValue("acceptorPattern", defaultAcceptorPattern);
	}
	
	public Set<String> getIgnoredAtomNames(CommandLine commandLine){
		String v = commandLine.getOptionValue("ignoreAtomNames");
		if(v==null) return new HashSet<>();
		return Stream.of(v.split(",")).collect(Collectors.toSet());
	}
	
}
