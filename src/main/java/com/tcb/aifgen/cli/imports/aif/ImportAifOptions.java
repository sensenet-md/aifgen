package com.tcb.aifgen.cli.imports.aif;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.tcb.aifgen.cli.CommonOptions;

public class ImportAifOptions extends CommonOptions {
	
	public ImportAifOptions(){
		super();

		this.addOption(
				Option.builder("i")
				.argName("FILE")
				.type(String.class)
				.hasArg()
				.desc("input .aif file")
				.required()
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("sieve")
				.type(String.class)
				.argName("INTEGER")
				.hasArg()
				.desc("(opt.) use every nth frame (default 1)")
				.build()
				);
		
		this.addOption(
				Option.builder(null)
				.longOpt("minAvg")
				.argName("FLOAT")
				.type(String.class)
				.hasArg()
				.desc("(opt.) minimum average for timeline (default -Inf)")
				.build()
				);
		
	}
	
	public Path getAifPath(CommandLine commandLine){
		return Paths.get(commandLine.getOptionValue("i"));
	}
	
	public Integer getSieve(CommandLine commandLine){
		String sieve = commandLine.getOptionValue("sieve");
		if(sieve==null) return 1;
		return Integer.valueOf(sieve);
	}
	
	public Double getMinAvg(CommandLine commandLine){
		String minAvg = commandLine.getOptionValue("minAvg");
		if(minAvg==null) return Double.NEGATIVE_INFINITY;
		return Double.valueOf(minAvg);
	}
}
