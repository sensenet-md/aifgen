package com.tcb.aifgen.cli;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.EnumUtils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.tcb.aifgen.cli.exports.ExportActionMode;
import com.tcb.aifgen.cli.imports.ImportActionMode;
import com.tcb.cytoscape.cyLib.util.MapUtil;

public class Modes {
	
	private static Map<String,ImportActionMode> importModes;
	private static Map<String,ExportActionMode> exportModes;
	private static Set<String> modeCommands;
	
	
	private static <T extends Enum<T>> String getCommandName(T en){
		return en.name().toLowerCase();
	}
	
	static {
		importModes = MapUtil.createMap(Arrays.asList(ImportActionMode.values()), (m) -> getCommandName(m));
		exportModes = MapUtil.createMap(Arrays.asList(ExportActionMode.values()), (m) -> getCommandName(m));
		modeCommands = new HashSet<>();
		modeCommands.addAll(importModes.keySet());
		modeCommands.addAll(exportModes.keySet());
		modeCommands.addAll(KeywordMode.getCommands());
	}
	
	public static Optional<ImportActionMode> getImportMode(String commandName){
		return Optional.ofNullable(importModes.getOrDefault(commandName,null));
	}
	
	public static Optional<ExportActionMode> getExportMode(String commandName){
		return Optional.ofNullable(exportModes.getOrDefault(commandName, null));
	}
	
	public static Optional<KeywordMode> getKeywordMode(String commandName){
		return Optional.ofNullable(EnumUtils.getEnum(KeywordMode.class, commandName.toUpperCase()));
	}
	
	public static Set<String> getModeCommands(){
		return modeCommands;
	}
	
	
}
