package com.tcb.aifgen.cli;

import java.util.List;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

public enum KeywordMode {
	DEBUG, DIFFERENCE_TO_REF;
	
	public static List<String> getCommands(){
		return Stream.of(KeywordMode.values())
				.map(v -> v.name().toLowerCase())
				.collect(ImmutableList.toImmutableList());
	}
}
