package com.tcb.aifgen.cli.exports.aif;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.apache.commons.cli.CommandLine;

import com.tcb.aifgen.cli.ArgumentParser;
import com.tcb.aifgen.cli.exports.ExportAction;
import com.tcb.aifgen.cli.exports.ExportOptions;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifWriter;
import com.tcb.aifgen.importer.aifImporter.ZaifWriter;

public class ExportZaifAction implements ExportAction {

	private CommandLine cmd;

	public ExportZaifAction(List<String> arguments){
		this.cmd = ArgumentParser.parseArguments(new ExportOptions(), arguments);
	}
	
	@Override
	public void export(InteractionImportData importData) throws IOException {
		Path outPath = ExportOptions.getPath(cmd);
		ZaifWriter writer = new ZaifWriter(importData.getInteractions(),importData.getTimelineType());
		writer.write(outPath);
	}

}
