package com.tcb.aifgen.cli.exports;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.tcb.aifgen.cli.CommonOptions;

public class ExportOptions extends CommonOptions {
	
	public ExportOptions(){
		super();

		this.addOption(
				Option.builder("o")
				.argName("FILE")
				.type(String.class)
				.hasArg()
				.desc("output file path")
				.required()
				.build()
				);		
	}
	
	public static Path getPath(CommandLine commandLine){
		return Paths.get(commandLine.getOptionValue("o"));
	}
}
