package com.tcb.aifgen.cli.exports;

import java.io.IOException;
import java.util.List;

import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;

public interface ExportAction {
	public void export(InteractionImportData importData) throws IOException;
}
