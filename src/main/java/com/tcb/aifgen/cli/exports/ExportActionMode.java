package com.tcb.aifgen.cli.exports;

import java.util.Arrays;
import java.util.List;

import com.tcb.aifgen.cli.exports.aif.ExportAifAction;
import com.tcb.aifgen.cli.exports.aif.ExportZaifAction;

public enum ExportActionMode {
	EXPORT_AIF,EXPORT_ZAIF;
	
	public ExportAction getAction(List<String> arguments){
		switch(this){
		case EXPORT_AIF: return new ExportAifAction(arguments);
		case EXPORT_ZAIF: return new ExportZaifAction(arguments);
		default: return null;
		}
	}
	
}
