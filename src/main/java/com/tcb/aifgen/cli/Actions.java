package com.tcb.aifgen.cli;

import java.util.List;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.tcb.aifgen.cli.exports.ExportAction;
import com.tcb.aifgen.cli.imports.ImportAction;

@AutoValue
public abstract class Actions {
	public static Actions create(
			List<ImportAction> importActions,
			List<ImportAction> importDifferenceActions,
			List<ExportAction> exportActions){
		return new AutoValue_Actions(
				ImmutableList.copyOf(importActions),
				ImmutableList.copyOf(importDifferenceActions),
				ImmutableList.copyOf(exportActions));
	}
	
	public abstract List<ImportAction> getImportActions();
	public abstract List<ImportAction> getImportDifferenceActions();
	public abstract List<ExportAction> getExportActions();
}
