package com.tcb.aifgen.cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class CommonOptions extends Options {
	
	public CommonOptions(){
		this.addOption(
				Option.builder("h")
				.type(String.class)
				.desc("print help and exit")
				.build());
	}
}
