package com.tcb.aifgen.cli;

import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.tcb.aifgen.Main;

public class ArgumentParser {
	public static CommandLine parseArguments(
			Options options,
			List<String> arguments){
		String commandName = arguments.get(0);
		arguments = arguments.subList(1, arguments.size());
		try{
			CommandLine cmd = new DefaultParser().parse(options,arguments.toArray(new String[0]));
			if(cmd.hasOption('h')) {
				printHelp(options,commandName);
				System.exit(1);
			}
			return cmd;
		} catch(ParseException ex){
			printHelp(options,commandName);
			System.out.println("Parsing error:");
			throw new RuntimeException(ex);
		}
	}
	
	protected static void printHelp(Options options, String commandName){
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(
				commandName,
				options, true);
	}
}
