package com.tcb.aifgen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.EnumUtils;

import com.google.common.collect.ImmutableList;
import com.tcb.aifgen.cli.Actions;
import com.tcb.aifgen.cli.KeywordMode;
import com.tcb.aifgen.cli.Modes;
import com.tcb.aifgen.cli.exports.ExportAction;
import com.tcb.aifgen.cli.exports.ExportActionMode;
import com.tcb.aifgen.cli.imports.ImportAction;
import com.tcb.aifgen.cli.imports.ImportActionMode;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.combinedImporter.CombinedInteractionImporter;
import com.tcb.aifgen.importer.differenceImporter.DifferenceImporter;
import com.tcb.cytoscape.cyLib.util.MapUtil;


public class Main {
	
	public static boolean debug = false;
	
	public static void main(String[] args) {
		try{
			Actions actions = getActions(args);
			
			List<InteractionImporter> importers = new ArrayList<>();
			List<InteractionImporter> differenceImporters = new ArrayList<>();
			for(ImportAction a:actions.getImportActions()){
				importers.add(a.read());
			}
			for(ImportAction a:actions.getImportDifferenceActions()){
				differenceImporters.add(a.read());
			}
					
			InteractionImporter importer = new CombinedInteractionImporter(importers);
			if(!differenceImporters.isEmpty()){
				InteractionImporter refImporter = new CombinedInteractionImporter(differenceImporters);
				importer = new DifferenceImporter(refImporter,importer);
			}
			
			
			InteractionImportData importData = importer.read();
			
			for(ExportAction a:actions.getExportActions()){
				a.export(importData);
			}			
		} catch(Exception e){
			System.out.println("An error occured during processing:");
			System.out.println(e.getClass());
			System.out.println("-----");
			System.out.println(e.getMessage());
			System.out.println("-----");
			if(debug) e.printStackTrace();
			else System.out.println("Run in debug mode for more information");
			System.exit(1);
		}		
	}
	
	
	private static Actions getActions(String[] args){
		List<List<String>> modeArguments = splitModeArguments(args);
		List<ImportAction> importActions = new ArrayList<>();
		List<ImportAction> differenceImportActions = new ArrayList<>();
		List<ExportAction> exportActions = new ArrayList<>();
		
		boolean pastDifferenceKeyword = false;
		
		for(List<String> argBlock:modeArguments){
			String modeCommand = argBlock.get(0);
			
			Optional<KeywordMode> keywordMode = Modes.getKeywordMode(modeCommand);
			if(keywordMode.isPresent()){
				KeywordMode mode = keywordMode.get();
				switch(mode){
				case DEBUG: debug = true; break;
				case DIFFERENCE_TO_REF: pastDifferenceKeyword = true; break;
				default: throw new UnsupportedOperationException();
				}
			}
			
			Optional<ImportActionMode> importMode = Modes.getImportMode(modeCommand);
			if(importMode.isPresent() && !pastDifferenceKeyword) {
				importActions.add(importMode.get().getAction(argBlock));
			}
			if(importMode.isPresent() && pastDifferenceKeyword){
				differenceImportActions.add(importMode.get().getAction(argBlock));
			}
			Optional<ExportActionMode> exportMode = Modes.getExportMode(modeCommand);
			if(exportMode.isPresent()){
				exportActions.add(exportMode.get().getAction(argBlock));
			}
		}
		
		Actions result = Actions.create(importActions,differenceImportActions,exportActions);
		return result;
	}
		
	private static List<List<String>> splitModeArguments(String[] args){
		List<List<String>> result = new ArrayList<>();
		List<Integer> modeIndices = new ArrayList<>(getModeIndices(args));
		if(modeIndices.isEmpty() || modeIndices.get(0) != 0) {
			printModeUsage();
			System.exit(1);
		}
		modeIndices.add(args.length);
		while(modeIndices.size() > 1){
			Integer start = modeIndices.remove(0);
			Integer end = modeIndices.get(0);
			List<String> modeArgs = Arrays.asList(args).subList(start, end);
			result.add(modeArgs);
		}
		return result;
	}
	
		
	private static List<Integer> getModeIndices(String[] args){
		Set<String> modes = Modes.getModeCommands();
		ImmutableList.Builder<Integer> result = ImmutableList.builder();
		for(int i=0;i<args.length;i++){
			String arg = args[i];
			if(modes.contains(arg)) result.add(i);
		}
		return result.build();
	}
		
	public static void printModeUsage(){
		System.out.println("usage: ${BIN} <mode> <mode-options>");
		System.out.println("to get help regarding a specific mode: ${BIN} <mode> -h");
		printAvailableModes();
	}
	
	public static void printAvailableModes(){
		String modes = Modes.getModeCommands().stream()
				.sorted()
				.collect(Collectors.joining(","));
		System.out.println(String.format("Available modes: %s",modes));
	}
}
