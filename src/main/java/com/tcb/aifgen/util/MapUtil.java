package com.tcb.aifgen.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.google.common.collect.Sets;
import com.tcb.atoms.residues.Residue;
import com.tcb.common.util.SafeMap;

public class MapUtil {
	public static <K,V> Map<K,V> merge(Map<K,V> mapA, Map<K,V> mapB){
		Set<K> intersectingKeys = Sets.intersection(mapA.keySet(), mapB.keySet());
		Map<K,V> result = new HashMap<>();
		
		for(K key:intersectingKeys){
			V valueA = mapA.get(key);
			V valueB = mapB.get(key);
			if(!valueA.equals(valueB)){
				throw new IllegalArgumentException("Maps contain contradictory values");
			}
		}
	
		result.putAll(mapA);
		result.putAll(mapB);
		
		SafeMap<K,V> safeResult = new SafeMap<>();
		safeResult.putAll(result);
		return safeResult;
	}
	
	public static <K,V> Map<K,V> merge(Collection<Map<K,V>> maps){
		return maps.stream()
				.reduce(new SafeMap<>(), (m1,m2) -> merge(m1,m2));
	}
	
	public static <K,V> V getOrPut(Map<K,V> map, K key, Supplier<V> defaultValueSupplier){
		if(map.containsKey(key)) return map.get(key);
		V defaultValue = defaultValueSupplier.get();
		map.put(key, defaultValue);
		return defaultValue;
	}
	
	public static <K,V> List<V> getAll(Map<K,V> map, List<K> keys){
		return keys.stream()
				.map(k -> map.get(k))
				.collect(Collectors.toList());
	}
	
}
