package com.tcb.aifgen.util;

public class ArrayUtil {
	public static float mean(float[] arr){
		float sum = 0f;
		for(float f:arr) sum+=f;
		return sum / arr.length;
	}
	
	public static double mean(double[] arr){
		float sum = 0f;
		for(double d:arr) sum+=d;
		return sum / arr.length;
	}
}
