package com.tcb.aifgen.util;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.util.MathUtils;

import com.tcb.aifgen.importer.pdbImporter.PdbAtom;

public class MathUtil {

	private static final double radToDegFactor = (180./Math.PI);
	
	public static double radToDeg(double rad){
		return rad * radToDegFactor;
	}
	
	public static double normalizeToPlusMinusPi(double angle){
		return MathUtils.normalizeAngle(angle, 0.0);
	}
	
	// Calculates the shortest angle between two vectors with (probably) range [0,180]
	public static double angleDeg(Vector3D v1, Vector3D v2){
		// Probably calculated by an acos function with possible results [0,pi]
		double angle = Vector3D.angle(v1, v2);
		return radToDeg(angle); 
	}
	
	// Calculates the shortest angle between three point vectors with v2 as the center
	public static double anglePointsDeg(Vector3D v1, Vector3D v2, Vector3D v3){
		v1 = v1.subtract(v2);
		v3 = v3.subtract(v2);
		return angleDeg(v1,v3);		
	}
		
}
