package com.tcb.aifgen.util;

public class StringUtil {
	public static String append(String base, String toAppend, int count){
		StringBuilder builder = new StringBuilder();
		for(int i=0;i<count;i++){
			builder.append(toAppend);
		}
		return base + builder.toString();
	}
	
	public static String getTrimmedSubstring(String s, int low, int high){
		return s.substring(low,high).trim();
	}
}
