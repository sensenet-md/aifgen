package com.tcb.aifgen.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {
	public static <T> List<T> subList(List<T> lst, List<Integer> indices){
		List<T> result = new ArrayList<>();
		for(Integer i:indices){
			result.add(lst.get(i));
		}
		return result;
	}
	
	public static <T> List<T> takeEvery(List<T> lst, Integer n){
		List<T> result = new ArrayList<>(lst.size() / n);
		for(int i=0;i<lst.size();i+=n){
			result.add(lst.get(i));
		}
		return result;
	}
}
