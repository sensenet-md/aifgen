package com.tcb.aifgen.util;

import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.tcb.common.util.SafeMap;

public class ResidueNameUtil {
	
	private static BiMap<String,String> nameMap;
	private static Map<String,String> threeLetterAliases;
	
	static {
		BiMap<String,String> map = HashBiMap.create();
		map.put("ALA", "A");
		map.put("ARG", "R");
		map.put("ASN", "N");
		map.put("ASP", "D");
		map.put("CYS", "C");
		map.put("GLU","E");
		map.put("GLN","Q");
		map.put("GLY","G");
		map.put("HIS", "H");
		map.put("ILE", "I");
		map.put("LEU", "L");
		map.put("LYS", "K");
		map.put("MET","M");
		map.put("PHE", "F");
		map.put("PRO", "P");
		map.put("SER", "S");
		map.put("THR", "T");
		map.put("TRP", "W");
		map.put("TYR", "Y");
		map.put("VAL", "V");
		nameMap = map;
	}
	
	static {
		Map<String,String> map = new HashMap<>();
		map.put("HIE", "HIS");
		map.put("HID", "HIS");
		threeLetterAliases = map;
	}
		
	public static String oneToThreeLetter(String s){
		return nameMap.inverse().getOrDefault(s, s);
	}
	
	public static String threeToOneLetter(String s){
		s = translateThreeLetter(s);
		return nameMap.getOrDefault(s, s);
	}
	
	private static String translateThreeLetter(String s){
		return threeLetterAliases.getOrDefault(s, s);
	}
}
