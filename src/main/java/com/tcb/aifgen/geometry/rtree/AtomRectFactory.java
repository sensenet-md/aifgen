package com.tcb.aifgen.geometry.rtree;

import com.conversantmedia.util.collection.geometry.Point3d;
import com.conversantmedia.util.collection.geometry.Rect3d;
import com.conversantmedia.util.collection.spatial.HyperPoint;
import com.conversantmedia.util.collection.spatial.HyperRect;
import com.conversantmedia.util.collection.spatial.RectBuilder;
import com.tcb.aifgen.importer.pdbImporter.PdbAtom;

public class AtomRectFactory implements RectBuilder<PdbAtom> {

	private final double faceDistance;

	public AtomRectFactory(double faceDistance){
		this.faceDistance = faceDistance;
	}
	
	@Override
	public HyperRect getBBox(PdbAtom atom) {
		double x = atom.getX();
		double y = atom.getY();
		double z = atom.getZ();
		double d = faceDistance;
		double xMin = x - d;
		double yMin = y - d;
		double zMin = z - d;
		double xMax = x + d;
		double yMax = y + d;
		double zMax = z + d;
		Rect3d box = new Rect3d(xMin,yMin,zMin,xMax,yMax,zMax);
		return box;
	}

	@Override
	public HyperRect getMbr(HyperPoint p1, HyperPoint p2) {
		Point3d _p1 = (Point3d)p1;
		Point3d _p2 = (Point3d)p2;
		double x1 = _p1.getCoord(Point3d.X);
		double y1 = _p1.getCoord(Point3d.Y);
		double z1 = _p1.getCoord(Point3d.Z);
		double x2 = _p2.getCoord(Point3d.X);
		double y2 = _p2.getCoord(Point3d.Y);
		double z2 = _p2.getCoord(Point3d.Z);
		Rect3d box = new Rect3d(x1,y1,z1,x2,y2,z2);
		return box;
	}
}
