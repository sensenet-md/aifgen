package com.tcb.aifgen.geometry.rtree;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.conversantmedia.util.collection.spatial.HyperRect;
import com.conversantmedia.util.collection.spatial.SpatialSearch;
import com.google.common.collect.ImmutableSet;
import com.tcb.aifgen.importer.pdbImporter.PdbAtom;
import com.tcb.common.util.SafeMap;

public class AtomSpatialSearch {
	private SpatialSearch<PdbAtom> search;
	private Iterable<PdbAtom> atoms;
	
	public static AtomSpatialSearch create(Iterable<PdbAtom> atoms){
		SpatialSearch<PdbAtom> search = new AtomRTreeFactory().create(atoms);
		return new AtomSpatialSearch(atoms,search);
	}
	
	private AtomSpatialSearch(Iterable<PdbAtom> atoms, SpatialSearch<PdbAtom> search){
		this.search = search;
		this.atoms = atoms;
	}
	
	public Set<PdbAtom> getInRange(PdbAtom atom, double distance){
		Set<PdbAtom> candidates = new HashSet<>();
		HyperRect box = new AtomRectFactory(distance).getBBox(atom);
		search.search(box, candidates);
		candidates.remove(atom);
		
		Set<PdbAtom> result = candidates.stream()
				.filter(a -> getDistance(atom,a) < distance)
				.collect(ImmutableSet.toImmutableSet());
		
		return result;
	}
	
	public Map<PdbAtom,Set<PdbAtom>> getRangeMap(double distance){
		Map<PdbAtom,Set<PdbAtom>> result = new SafeMap<>();
		for(PdbAtom atom:atoms){
			result.put(atom, getInRange(atom,distance));
		}
		return result;
	}
	
	private double getDistance(PdbAtom a, PdbAtom b){
		Vector3D vA = a.getVector();
		Vector3D vB = b.getVector();
		return vA.distance(vB);
	}
}
