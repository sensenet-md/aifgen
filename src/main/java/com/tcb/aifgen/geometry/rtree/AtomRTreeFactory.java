package com.tcb.aifgen.geometry.rtree;

import java.util.List;

import com.conversantmedia.util.collection.geometry.Point3d;
import com.conversantmedia.util.collection.geometry.Rect3d;
import com.conversantmedia.util.collection.spatial.HyperPoint;
import com.conversantmedia.util.collection.spatial.HyperRect;
import com.conversantmedia.util.collection.spatial.RTree;
import com.conversantmedia.util.collection.spatial.RectBuilder;
import com.conversantmedia.util.collection.spatial.SpatialSearch;
import com.conversantmedia.util.collection.spatial.SpatialSearches;
import com.tcb.aifgen.importer.pdbImporter.PdbAtom;

public class AtomRTreeFactory {

	public SpatialSearch<PdbAtom> create(Iterable<PdbAtom> atoms){
		SpatialSearch<PdbAtom> tree = SpatialSearches.rTree(new AtomRectFactory(0.0));
		atoms.forEach(a -> tree.add(a));
		return tree;
	}
	
	
}
