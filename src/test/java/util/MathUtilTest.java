package util;

import static com.tcb.aifgen.util.MathUtil.angleDeg;
import static com.tcb.aifgen.util.MathUtil.anglePointsDeg;
import static org.junit.Assert.*;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Before;
import org.junit.Test;

public class MathUtilTest {

	private Vector3D v1;
	private Vector3D v2;
	private Vector3D v1i;
	private Vector3D v2i;


	@Before
	public void setUp() throws Exception {
		this.v1 = new Vector3D(1,0,0);
		this.v2 = new Vector3D(0,1,0);
		this.v1i = new Vector3D(-1,0,0);
		this.v2i = new Vector3D(0,-1,0);
	}

	@Test
	public void testAngleDeg(){
		assertEquals(0.,angleDeg(v1, v1),0.01);
		
		assertEquals(90.,angleDeg(v1, v2),0.01);
		assertEquals(180.,angleDeg(v1, v1i),0.01);
		
		assertEquals(90.,angleDeg(v1, v2i),0.01);
	}
	
	@Test
	public void testAnglePointsDeg(){
		Vector3D p1 = new Vector3D(0,0,0);
		Vector3D p2 = new Vector3D(1,0,0);
		Vector3D p3 = new Vector3D(2,0,0);
		 
		assertEquals(180.0,anglePointsDeg(p1,p2,p3),0.01);
		
		p1 = new Vector3D(0,0,0);
		p2 = new Vector3D(1,1./Math.sqrt(3),0);
		p3 = new Vector3D(2,0,0);
		 
		assertEquals(120.0,anglePointsDeg(p1,p2,p3),0.01);
		
		p1 = new Vector3D(0,0,0);
		p2 = new Vector3D(1,1,0);
		p3 = new Vector3D(2,0,0);
		
		assertEquals(90.0,anglePointsDeg(p1,p2,p3),0.01);
		
		p1 = new Vector3D(0,0,0);
		p2 = new Vector3D(1,Math.sqrt(3),0);
		p3 = new Vector3D(2,0,0);
		 
		assertEquals(60.0,anglePointsDeg(p1,p2,p3),0.01);
				
	}
	
}
