package util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.aifgen.util.ListUtil;

public class ListUtilTest {

	private List<Integer> lst;

	@Before
	public void setUp() throws Exception {
		this.lst = Arrays.asList(10,11,12,13,14,15,16);
	}

	@Test
	public void testSubList() {
		List<Integer> r = ListUtil.subList(lst, Arrays.asList(2,5,1));
		assertEquals(Arrays.asList(12,15,11),r);
	}

	@Test
	public void testTakeEvery() {
		List<Integer> r = ListUtil.takeEvery(lst, 3);
		assertEquals(Arrays.asList(10,13,16),r);
	}

}
