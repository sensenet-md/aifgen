package gemoetry.rtree;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.conversantmedia.util.collection.geometry.Rect3d;
import com.conversantmedia.util.collection.spatial.HyperRect;
import com.conversantmedia.util.collection.spatial.SpatialSearch;
import com.tcb.aifgen.geometry.rtree.AtomRTreeFactory;
import com.tcb.aifgen.geometry.rtree.AtomRectFactory;
import com.tcb.aifgen.importer.pdbImporter.PdbAtom;

public class AtomRTreeFactoryTest {
	
	private PdbAtom a1 = PdbAtom.create(
			0, "", "", 0, "", "", "", "", 1.0, 1.0, 1.0);
	private PdbAtom a2 = PdbAtom.create(
			1, "", "", 0, "", "", "", "", 5.0, 5.0, 5.0);
	private PdbAtom a3 = PdbAtom.create(
			2, "", "", 0, "", "", "", "", 1.5, 1.0, 1.0);
	private PdbAtom a4 = PdbAtom.create(
			3, "", "", 0, "", "", "", "", 1.5, 2.0, 1.0);
	private PdbAtom a5 = PdbAtom.create(
			4, "", "", 0, "", "", "", "", 0.6, 0.4, 0.7);
	
	private AtomRTreeFactory fac;
	private List<PdbAtom> atoms = Arrays.asList(a1,a2,a3,a4,a5);
	
	@Before
	public void setUp() throws Exception {
		this.fac = new AtomRTreeFactory();
	}
	
	@Test
	public void testCreate() {
		 SpatialSearch<PdbAtom> test = fac.create(atoms);
		
		 List<PdbAtom> result = new ArrayList<>();
		 		 
		 HyperRect box = new AtomRectFactory(0.49).getBBox(a1);
		 test.search(box, result);
		 assertEquals(Arrays.asList(a1),result);
		 result.clear();
		 
		 box = new AtomRectFactory(0.5).getBBox(a1);
		 test.search(box, result);
		 assertEquals(Arrays.asList(a1,a3),result);
		 result.clear();
		 
		 box = new AtomRectFactory(0.6).getBBox(a1);
		 test.search(box, result);
		 assertEquals(Arrays.asList(a1,a3,a5),result);
		 result.clear();
		 
		 box = new AtomRectFactory(2.0).getBBox(a1);
		 test.search(box, result);
		 assertEquals(Arrays.asList(a1,a3,a4,a5),result);
		 result.clear();
		 
		 box = new AtomRectFactory(10.0).getBBox(a1);
		 test.search(box, result);
		 assertEquals(atoms,result);
		 result.clear();
	}

}
