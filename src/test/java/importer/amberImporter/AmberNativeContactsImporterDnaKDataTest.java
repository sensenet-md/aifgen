package importer.amberImporter;

import static org.junit.Assert.*;

import java.net.URL;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import importerTestData.DnaKPdbContactsData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.amberImporter.AmberNativeContactsImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbContactImporter;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

import importer.AbstractImporterTest;

public class AmberNativeContactsImporterDnaKDataTest extends AbstractImporterTest {

	private Path amberContactsPath;
	private List<Path> amberTimeSeriesPaths;
	private Path amberPdbPath;
	private RefData refData;
	private Path brokenPdbPath;


	@Before
	public void setUp() throws Exception {
		this.refData = new DnaKPdbContactsData();
		ClassLoader classLoader = getClass().getClassLoader();
		this.amberContactsPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "contacts.out");
		this.amberTimeSeriesPaths = 
				Arrays.asList(
						PathUtil.fromResource(
								classLoader, refData.getBaseResourcePath() + "contacts.series"));
		this.amberPdbPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "contacts.pdb");
		this.brokenPdbPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "contacts.broken.pdb");
		super.setUp();
	}

	@Override
	public InteractionImporter createImporter() {
		return new AmberNativeContactsImporter(amberContactsPath,amberTimeSeriesPaths,amberPdbPath);
	}
	
	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		Set<String> ignoredAtoms = new HashSet<>();
		Integer sieve = 1;
		Double minAvg = Double.NEGATIVE_INFINITY;
		return new AmberNativeContactsImporter(
				amberContactsPath,amberTimeSeriesPaths,amberPdbPath,type,ignoredAtoms,false,
				sieve,minAvg);
	}

	@Override
	public RefData getRefData() {
		return refData;
	}

	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"contacts.out");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFailsForDuplicateTimelines() throws Exception{
		List<Path> timeSeriesPaths = Arrays.asList(amberTimeSeriesPaths.get(0),amberTimeSeriesPaths.get(0));
		InteractionImporter importer = new AmberNativeContactsImporter(amberContactsPath,timeSeriesPaths,amberPdbPath);
		importer.read();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFailsForBrokenPdb()  throws Exception{
		InteractionImporter importer = new AmberNativeContactsImporter(amberContactsPath,amberTimeSeriesPaths,brokenPdbPath);
		importer.read();
	}
	
	@Test
	public void testNoBBInteractions()  throws Exception {
		Set<String> refAtomNames = getAtomNames(imported);
		assertTrue(refAtomNames.contains("N"));
		assertTrue(refAtomNames.contains("O"));
		
		Set<String> ignoreAtoms = new HashSet<>();
		ignoreAtoms.addAll(Arrays.asList("O","N"));
		Integer sieve = 1;
		Double minAvg = Double.NEGATIVE_INFINITY;
		
		InteractionImporter testImporter = 
				new AmberNativeContactsImporter(
						amberContactsPath,amberTimeSeriesPaths,amberPdbPath,
						AmberNativeContactsImporter.getDefaultInteractionType(),
						ignoreAtoms,false,sieve,minAvg);

		Set<String> atomNames = getAtomNames(testImporter.read());
		
		assertFalse(atomNames.contains("N"));
		assertFalse(atomNames.contains("O"));
	}
	
	@Test
	public void testNoIntraResidueInteractions() throws Exception{
		Atom source = Atom.create("OD1",7, "ASP", "", "", "");
		Atom target = Atom.create("OD2",7, "ASP", "", "", "");
		Interaction testInteraction = Interaction.create(
				source, target, 
				Arrays.asList(),
				Timeline.create(Arrays.asList(1)),
				InteractionType.CONTACT.toString());
		
		assertTrue(interactions.contains(testInteraction));
		Integer sieve = 1;
		Double minAvg = Double.NEGATIVE_INFINITY;
		
		InteractionImporter testImporter = 
				new AmberNativeContactsImporter(
						amberContactsPath,amberTimeSeriesPaths,amberPdbPath,
						AmberNativeContactsImporter.getDefaultInteractionType(),
						new HashSet<>(),true,sieve,minAvg);
		
		List<Interaction> testInteractions = testImporter.read().getInteractions();
		
		assertFalse(testInteractions.isEmpty());
		assertFalse(testInteractions.contains(testInteraction));
	}
	
	@Test
	public void testSieve()  throws Exception {
		Integer sieve = 2;
		Double minAvg = Double.NEGATIVE_INFINITY;
		Set<String> ignoreAtoms = new HashSet<>();
		
		InteractionImporter testImporter = 
				new AmberNativeContactsImporter(
						amberContactsPath,amberTimeSeriesPaths,amberPdbPath,
						AmberNativeContactsImporter.getDefaultInteractionType(),ignoreAtoms,false,
						sieve,minAvg);
		
		for(Interaction in:testImporter.read().getInteractions()){
			assertEquals(1,(int)in.getTimeline().getLength());
		}

	}
	
	@Test
	public void testMinAvg()  throws Exception {
		Integer sieve = 1;
		Double minAvg = 0.05;
		Set<String> ignoreAtoms = new HashSet<>();
		
		InteractionImporter testImporter = 
				new AmberNativeContactsImporter(
						amberContactsPath,amberTimeSeriesPaths,amberPdbPath,
						AmberNativeContactsImporter.getDefaultInteractionType(),ignoreAtoms,false,
						sieve,minAvg);
		
		List<Interaction> interactions = testImporter.read().getInteractions();
		
		assertEquals(1859,(int)interactions.size());
		
		for(Interaction in:interactions){
			float sum = 0f;
			for(double d:in.getTimeline().getData()) sum += d;
			double avg = sum / in.getTimeline().getLength();
			assertTrue(avg >= minAvg);
		}
		
	}
	
	
	
	/*public void testCustomInteractionType() throws Exception {
		InteractionImporter testImporter = 
				new AmberNativeContactsImporter(
						amberContactsPath,amberTimeSeriesPaths,amberPdbPath,
						"customType",
						new HashSet<>(),true);
				
		for(Interaction i:testImporter.getInteractions()){
			assertEquals("customType",i.getType());
		}
	}*/

	

	

}
