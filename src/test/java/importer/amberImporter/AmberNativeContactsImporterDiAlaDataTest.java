package importer.amberImporter;

import static org.junit.Assert.*;

import java.net.URL;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import importerTestData.DiAlaTrajContactsData;
import importerTestData.DnaKPdbContactsData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.amberImporter.AmberNativeContactsImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbContactImporter;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

import importer.AbstractImporterTest;

public class AmberNativeContactsImporterDiAlaDataTest extends AbstractImporterTest {

	private Path amberContactsPath;
	private List<Path> amberTimeSeriesPaths;
	private Path amberPdbPath;
	private RefData refData;

	@Override
	public void setUp() throws Exception {
		this.refData = new DiAlaTrajContactsData();
		ClassLoader classLoader = getClass().getClassLoader();
		this.amberContactsPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "contacts.out");
		this.amberTimeSeriesPaths = 
				Arrays.asList(
						PathUtil.fromResource(
								classLoader, refData.getBaseResourcePath() + "contacts.series"),
						PathUtil.fromResource(
								classLoader, refData.getBaseResourcePath() + "contacts.nonnative.series"));
		this.amberPdbPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "contacts.pdb");
		super.setUp();
	}

	@Override
	public RefData getRefData() {
		return refData;
	}

	@Override
	public InteractionImporter createImporter() {
		return new AmberNativeContactsImporter(amberContactsPath,amberTimeSeriesPaths,amberPdbPath);
	}
	
	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		Set<String> ignoredAtoms = new HashSet<>();
		Integer sieve = 1;
		Double minAvg = Double.NEGATIVE_INFINITY;
		return new AmberNativeContactsImporter(
				amberContactsPath,amberTimeSeriesPaths,amberPdbPath,type,ignoredAtoms,false,
				sieve,minAvg);
	}

	@Override
	public void testGetName() {
		assertEquals(importer.getName(),"contacts.out");
	}
	
	@Test
	public void testSieve()  throws Exception {
		Integer sieve = 2;
		Double minAvg = Double.NEGATIVE_INFINITY;
		Set<String> ignoreAtoms = new HashSet<>();
		
		InteractionImporter testImporter = 
				new AmberNativeContactsImporter(
						amberContactsPath,amberTimeSeriesPaths,amberPdbPath,
						AmberNativeContactsImporter.getDefaultInteractionType(),ignoreAtoms,false,
						sieve,minAvg);
		
		for(Interaction in:testImporter.read().getInteractions()){
			assertEquals(25,(int)in.getTimeline().getLength());
		}

	}
	
	@Test
	public void testMinAvg()  throws Exception {
		Integer sieve = 1;
		Double minAvg = 0.05;
		Set<String> ignoreAtoms = new HashSet<>();
		
		InteractionImporter testImporter = 
				new AmberNativeContactsImporter(
						amberContactsPath,amberTimeSeriesPaths,amberPdbPath,
						AmberNativeContactsImporter.getDefaultInteractionType(),ignoreAtoms,false,
						sieve,minAvg);
		
		List<Interaction> interactions = testImporter.read().getInteractions();
		
		assertEquals(11,(int)interactions.size());
		
		for(Interaction in:interactions){
			float sum = 0f;
			for(double d:in.getTimeline().getData()) sum += d;
			double avg = sum / in.getTimeline().getLength();
			assertTrue(avg >= minAvg);
		}
		
	}

	
}
