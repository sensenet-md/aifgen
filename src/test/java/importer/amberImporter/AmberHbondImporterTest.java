package importer.amberImporter;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.amberImporter.AmberNativeContactsImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbContactImporter;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;

import importer.AbstractImporterTest;

public class AmberHbondImporterTest extends AbstractImporterTest {

	private Path hbondsPath;
	private Path hbondSeriesPath;
	private RefData refData;

	@Before
	public void setUp() throws Exception {
		this.refData = new UhrfTrajHbondsData();
		ClassLoader classLoader = getClass().getClassLoader();
		this.hbondsPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "hbonds.out");
		this.hbondSeriesPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "hbonds.series");
		super.setUp();
	}

	@Override
	public InteractionImporter createImporter() {
		return new AmberHbondImporter(hbondsPath,hbondSeriesPath);
	}
	
	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		Set<String> ignoredAtoms = new HashSet<>();
		Integer sieve = 1;
		Double minAvg = Double.NEGATIVE_INFINITY;
		return new AmberHbondImporter(hbondsPath,hbondSeriesPath,type, ignoredAtoms, sieve, minAvg);
	}

	@Override
	public RefData getRefData() {
		return refData;
	}
	
	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"hbonds.out");
	}
	
	@Test
	public void testNoBBInteractions()  throws Exception {
		Integer sieve = 1;
		Double minAvg = Double.NEGATIVE_INFINITY;
		
		Set<String> refAtomNames = getAtomNames(imported);
		assertTrue(refAtomNames.contains("N"));
		assertTrue(refAtomNames.contains("O"));
		
		Set<String> ignoreAtoms = new HashSet<>();
		ignoreAtoms.addAll(Arrays.asList("O","N"));
		
		InteractionImporter testImporter = 
				new AmberHbondImporter(hbondsPath,hbondSeriesPath,
						AmberHbondImporter.getDefaultInteractionType(), ignoreAtoms,
						sieve, minAvg);

		Set<String> atomNames = getAtomNames(testImporter.read());
				
		assertFalse(atomNames.contains("N"));
		assertFalse(atomNames.contains("O"));
	}
	
	@Test
	public void testSieve()  throws Exception {
		Integer sieve = 2;
		Double minAvg = Double.NEGATIVE_INFINITY;
		Set<String> ignoreAtoms = new HashSet<>();
		
		InteractionImporter testImporter = 
				new AmberHbondImporter(hbondsPath,hbondSeriesPath,
						AmberHbondImporter.getDefaultInteractionType(), ignoreAtoms,
						sieve, minAvg);
		
		for(Interaction in:testImporter.read().getInteractions()){
			assertEquals(101,(int)in.getTimeline().getLength());
		}

	}
	
	@Test
	public void testMinAvg()  throws Exception {
		Integer sieve = 1;
		Double minAvg = 0.05;
		Set<String> ignoreAtoms = new HashSet<>();
		
		InteractionImporter testImporter = 
				new AmberHbondImporter(hbondsPath,hbondSeriesPath,
						AmberHbondImporter.getDefaultInteractionType(), ignoreAtoms,
						sieve, minAvg);
		
		List<Interaction> interactions = testImporter.read().getInteractions();
		
		assertEquals(377,(int)interactions.size());
		
		for(Interaction in:interactions){
			float sum = 0f;
			for(double d:in.getTimeline().getData()) sum += d;
			double avg = sum / in.getTimeline().getLength();
			assertTrue(avg >= minAvg);
		}
		
	}


}
