package importer.differenceImporter;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.common.util.Tuple;

import importer.AbstractImporterTest;
import importerTestData.DifferenceUhrfTrajHbondsData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.aifImporter.OldMatImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.differenceImporter.DifferenceImporter;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;

public class DifferenceImporterTest extends AbstractImporterTest {

	private Path matPathReference;
	private Path matPathCompared;
	private DifferenceUhrfTrajHbondsData refData;


	@Before
	public void setUp() throws Exception {
		this.refData = new DifferenceUhrfTrajHbondsData();
		ClassLoader classLoader = getClass().getClassLoader();
		this.matPathReference = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + 
				"hbonds.mat");
		this.matPathCompared = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + 
				"hbonds2.mat");
		super.setUp();
		
	}
	
	@Override
	public InteractionImporter createImporter() {
		InteractionImporter referenceImporter = new OldMatImporter(matPathReference);
		InteractionImporter comparedImporter = new OldMatImporter(matPathCompared);
		return new DifferenceImporter(referenceImporter, comparedImporter);
		
	}
	
	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		throw new UnsupportedOperationException();
	}

	@Override
	public RefData getRefData() {
		return refData;
	}
	
	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"hbonds2.mat-hbonds.mat-diff");
	}
	
	@Ignore
	@Test
	public void testNoDoubleInteractions(){
		fail("Double interactions may happen in difference networks."
				+ "This test should be ignored.");
	}
	
	@Test
	public void testMutationMapContents(){
		Residue residue = Residue.create(215, "CYT", "", "", "?");
		assertEquals(new Tuple<String,String>("CYT","5CAC"), imported.getMutationMap().get(residue));
		assertEquals(1,imported.getMutationMap().size());
	}
	
	@Test
	public void testMutatedResidueNotPresent(){
		Residue residue = Residue.create(215, "CYT", "", "", "?");
		Set<Residue> residues = InteractionList.getInteractingResidues(interactions);
		assertTrue(residues.contains(residue));
		Residue mutatedResidue = Residue.create(215, "5CAC", "", "", "?");
		assertFalse(residues.contains(mutatedResidue));
	}
	
	@Test
	public void testMutatedAtomNotPresent(){
		Residue residue = Residue.create(215, "CYT", "", "", "?");
		Atom atom = Atom.create("O2", residue);
		Set<Atom> atoms = InteractionList.getInteractingAtoms(interactions);
		assertTrue(atoms.contains(atom));
		Residue mutatedResidue = Residue.create(215, "5CAC", "", "", "?");
		Atom mutatedAtom = Atom.create("O52", mutatedResidue);
		assertFalse(atoms.contains(mutatedAtom));
	}
	
	@Override
	public void testWithCustomInteractionType(){
		// Ignore
	}

	
	
}
