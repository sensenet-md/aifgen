package importer.pdbImporter;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import importerTestData.DnaKPdbContactsData;
import importerTestData.PdzPdbContactsData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.amberImporter.AmberNativeContactsImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbContactImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbHbondImporter;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;

import importer.AbstractImporterTest;

public class MultiPdbContactImporterTest extends AbstractImporterTest {

	private Path pdbPath;
	private RefData refData;
	private Path amberOutPath;
	private Path amberSeriesPath;
	private Path amberPdbPath;
	
	@Before
	public void setUp() throws Exception {
		this.refData = new PdzPdbContactsData();
		ClassLoader classLoader = getClass().getClassLoader();
		this.pdbPath = PathUtil.fromResource(classLoader,refData.getBaseResourcePath() + "3pdz.noChain.pdb");
		this.amberOutPath = PathUtil.fromResource(classLoader,refData.getBaseResourcePath() + "contacts.out");
		this.amberSeriesPath = PathUtil.fromResource(classLoader,refData.getBaseResourcePath() + "contacts.series");
		this.amberPdbPath = PathUtil.fromResource(classLoader,refData.getBaseResourcePath() + "contacts.pdb");
		super.setUp();
	}
	
	@Override
	public RefData getRefData() {
		return refData;
	}
	
	@Override
	public InteractionImporter createImporter() {
		return new PdbContactImporter(pdbPath,3.0,"O.*|N.*");
	}
	
	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"3pdz.noChain.pdb");
	}
	
	@Test
	public void testNoBBInteractions()  throws Exception {
		Set<String> refAtomNames = getAtomNames(imported);
		assertTrue(refAtomNames.contains("N"));
		assertTrue(refAtomNames.contains("O"));
		
		Set<String> ignoreAtoms = new HashSet<>();
		ignoreAtoms.addAll(Arrays.asList("O","N"));
		
		InteractionImporter testImporter = 
				new PdbContactImporter(pdbPath,3.0,"O.*|N.*",InteractionType.CONTACT.toString(),ignoreAtoms,false);
		
		Set<String> atomNames = getAtomNames(testImporter.read());
		
		assertFalse(atomNames.contains("N"));
		assertFalse(atomNames.contains("O"));
	}
	
	@Test
	public void testNoIntraResidueInteractions()  throws Exception {
		Interaction testInteraction = refData.getTestInteraction();
		
		assertTrue(interactions.contains(testInteraction));
		
		InteractionImporter testImporter = 
				new PdbContactImporter(pdbPath,3.0,"O.*|N.*",
						InteractionType.CONTACT.toString(),new HashSet<>(),true);
		
		List<Interaction> testInteractions = testImporter.read().getInteractions();
		
		assertFalse(testInteractions.isEmpty());
		assertFalse(testInteractions.contains(testInteraction));
	}
		
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyPattern() throws Exception {
		InteractionImporter test = new PdbContactImporter(pdbPath,3.0," ");
		test.read();
	}
	
	@Test
	public void testNoHydrogensInContacts() throws Exception {
		InteractionImporter test = new PdbContactImporter(pdbPath,3.0,".*");
		
		Set<String> atomNames = getAtomNames(test.read());
		
		for(String name:atomNames){
			assertFalse(name.startsWith("H"));
		}
	}
	
	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		return new PdbContactImporter(pdbPath,3.0,"O.*|N.*","customType",new HashSet<>(),true);
	}
	
	/* Currently not working, possibly due to bug in CPPTRAJ nativecontacts
	@Test
	public void comparePdbWithAmberImporter() throws IOException {
		
		AmberNativeContactsImporter amberImporter = new AmberNativeContactsImporter(
				this.amberOutPath,Arrays.asList(this.amberSeriesPath),this.amberPdbPath);
				
		InteractionImportData amberImportData = amberImporter.read();
		InteractionImportData pdbImportData = createImporter().read();
		
		Set<Interaction> amberInteractions = new HashSet<>(amberImportData.getInteractions());
		Set<Interaction> pdbInteractions = new HashSet<>(pdbImportData.getInteractions());
		
		System.out.println(amberInteractions.size());
		System.out.println(pdbInteractions.size());
		
		System.out.println(amberInteractions.stream()
				.sorted((i1,i2) -> i1.toString().compareTo(i2.toString()))
				.limit(10)
				.collect(Collectors.toList()));
		System.out.println(pdbInteractions.stream()
				.sorted((i1,i2) -> i1.toString().compareTo(i2.toString()))
				.limit(10)
				.collect(Collectors.toList()));
		assertTrue(amberInteractions.equals(pdbInteractions));
	}
	*/

}
