package importer.pdbImporter;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.tcb.aifgen.importer.pdbImporter.PdbAtom;
import com.tcb.aifgen.importer.pdbImporter.PdbReader;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;

public class PdbReaderTest {

	private PdbReader pdb;
	private Path pdbPath;
	private Path nmrPdbPath;
	private PdbReader nmrPdb;

	@Before
	public void setUp() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		this.pdbPath = PathUtil.fromResource(classLoader,"atomInteractionList/PdbReader/1o1m/1o1m.pdb");
		this.pdb = new PdbReader(pdbPath);
	}

	@Test
	public void testGetAtom()  throws Exception {
		pdb.init();
		Map<Integer,PdbAtom> atoms = pdb.getAtoms(0);
		
		PdbAtom atom = atoms.get(2);
		
		assertEquals(20.614,atom.getX(),0.001);
	}
	
	@Test
	public void testGetAtomWithAltLoc()  throws Exception {
		pdb.init();
		
		Map<Integer,PdbAtom> atoms = pdb.getAtoms(0);
		
		PdbAtom atom = atoms.get(1865);
		assertEquals("A", atom.getResidue().getAltLoc());
		
		atom = atoms.get(1866);
		assertEquals("B", atom.getResidue().getAltLoc());
	}

}
