package importer.pdbImporter;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import importerTestData.DnaKPdbHbondsData;
import importerTestData.PdzPdbHbondsData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.google.common.collect.Sets;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.amberImporter.AmberNativeContactsImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbContactImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbHbondImporter;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;

import importer.AbstractImporterTest;

public class MultiPdbHbondImporterTest extends AbstractImporterTest {

	private Path pdbPath;
	private Path amberAvgPath;
	private Path timeSeriesPath;
	private RefData refData;

	@Before
	public void setUp() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		this.refData = new PdzPdbHbondsData();
		
		this.pdbPath = PathUtil.fromResource(classLoader,refData.getBaseResourcePath() + "3pdz.noChain.pdb");
		this.amberAvgPath = PathUtil.fromResource(classLoader,refData.getBaseResourcePath() + "hbonds.out");
		this.timeSeriesPath = PathUtil.fromResource(classLoader,refData.getBaseResourcePath() + "hbonds.series");		
		super.setUp();
	}
	
	@Override
	public InteractionImporter createImporter() {
		return new PdbHbondImporter(pdbPath,3.0,120.0,"N.*","O.*|N.*");
	}
	
	@Override
	public RefData getRefData() {
		return refData;
	}
	
	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"3pdz.noChain.pdb");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyPattern() throws Exception {
		InteractionImporter test = new PdbHbondImporter(pdbPath,3.0,120.0,"","");
		test.read();
	}
	
	@Test
	public void testNoHydrogensInDonorsAcceptors() throws Exception {
		InteractionImporter test = new PdbHbondImporter(pdbPath,3.0,120.0,".*",".*");
		Set<String> atomNames = getAtomNames(test.read());
		
		for(String name:atomNames){
			assertFalse(name.startsWith("H"));
		}
	}

	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		return new PdbHbondImporter(pdbPath,3.0,120.0,".*",".*",type,new HashSet<>());
	}
	
	@Test
	public void testNoBBInteractions()  throws Exception {
		Set<String> refAtomNames = getAtomNames(imported);
		assertTrue(refAtomNames.contains("N"));
		assertTrue(refAtomNames.contains("O"));
		
		Set<String> ignoreAtoms = new HashSet<>();
		ignoreAtoms.addAll(Arrays.asList("O","N"));
		
		InteractionImporter testImporter = 
				new PdbHbondImporter(
						pdbPath,3.0,120.0,
						"F.*|O.*|N.*","F.*|O.*|N.*",
						PdbHbondImporter.getDefaultInteractionType(),
						ignoreAtoms);

		Set<String> atomNames = getAtomNames(testImporter.read());
		
		assertFalse(atomNames.contains("N"));
		assertFalse(atomNames.contains("O"));
	}
	

	@Test
	public void comparePdbWithHydrogensWithAmberImporter() throws IOException {
		
		AmberHbondImporter amberImporter = new AmberHbondImporter(this.amberAvgPath,this.timeSeriesPath);
				
		InteractionImportData amberImportData = amberImporter.read();
		InteractionImportData pdbImportData = createImporter().read();
		
		Set<Interaction> amberInteractions = new HashSet<>(amberImportData.getInteractions());
		Set<Interaction> pdbInteractions = new HashSet<>(pdbImportData.getInteractions());
				
		assertTrue(amberInteractions.equals(pdbInteractions));
	}

	

	
		
	
	
}
