package importer.pdbImporter;

import static org.junit.Assert.*;

import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import importerTestData.DnaKPdbContactsData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.amberImporter.AmberNativeContactsImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbContactImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbHbondImporter;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;

import importer.AbstractImporterTest;

public class PdbContactImporterTest extends AbstractImporterTest {

	private Path pdbPath;
	private RefData refData;
	
	@Before
	public void setUp() throws Exception {
		this.refData = new DnaKPdbContactsData();
		ClassLoader classLoader = getClass().getClassLoader();
		this.pdbPath = PathUtil.fromResource(classLoader,refData.getBaseResourcePath() + "contacts.pdb");
		super.setUp();
	}
	
	@Override
	public RefData getRefData() {
		return refData;
	}
	
	@Override
	public InteractionImporter createImporter() {
		return new PdbContactImporter(pdbPath,3.0,"O.*|N.*");
	}
	
	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"contacts.pdb");
	}
	
	@Test
	public void testNoBBInteractions()  throws Exception {
		Set<String> refAtomNames = getAtomNames(imported);
		assertTrue(refAtomNames.contains("N"));
		assertTrue(refAtomNames.contains("O"));
		
		Set<String> ignoreAtoms = new HashSet<>();
		ignoreAtoms.addAll(Arrays.asList("O","N"));
		
		InteractionImporter testImporter = 
				new PdbContactImporter(pdbPath,3.0,"O.*|N.*",InteractionType.CONTACT.toString(),ignoreAtoms,false);
		
		Set<String> atomNames = getAtomNames(testImporter.read());
		
		assertFalse(atomNames.contains("N"));
		assertFalse(atomNames.contains("O"));
	}
	
	@Test
	public void testNoIntraResidueInteractions()  throws Exception {
		Atom source = Atom.create("OD1",7, "ASP", "", "", "");
		Atom target = Atom.create("OD2",7, "ASP", "", "", "");
		Interaction testInteraction = Interaction.create(
				source, target, 
				Arrays.asList(),
				Timeline.create(Arrays.asList(1)),
				InteractionType.CONTACT.toString());
		
		assertTrue(interactions.contains(testInteraction));
		
		InteractionImporter testImporter = 
				new PdbContactImporter(pdbPath,3.0,"O.*|N.*",InteractionType.CONTACT.toString(),new HashSet<>(),true);
		
		List<Interaction> testInteractions = testImporter.read().getInteractions();
		
		assertFalse(testInteractions.isEmpty());
		assertFalse(testInteractions.contains(testInteraction));
	}
		
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyPattern() throws Exception {
		InteractionImporter test = new PdbContactImporter(pdbPath,3.0," ");
		test.read();
	}
	
	@Test
	public void testNoHydrogensInContacts() throws Exception {
		InteractionImporter test = new PdbContactImporter(pdbPath,3.0,".*");
		
		Set<String> atomNames = getAtomNames(test.read());
		
		for(String name:atomNames){
			assertFalse(name.startsWith("H"));
		}
	}
	
	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		return new PdbContactImporter(pdbPath,3.0,"O.*|N.*","customType",new HashSet<>(),true);
	}

}
