package importer.pdbImporter;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.tcb.aifgen.importer.pdbImporter.PdbAtom;
import com.tcb.aifgen.importer.pdbImporter.PdbReader;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;

public class MultiPdbReaderTest {

	private PdbReader pdb;
	private Path pdbPath;
	private Path brokenPdbPath;

	@Before
	public void setUp() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		this.pdbPath = PathUtil.fromResource(classLoader,"atomInteractionList/PdbReader/3pdz/3pdz.pdb");
		this.brokenPdbPath = PathUtil.fromResource(classLoader, 
				"atomInteractionList/PdbReader/3pdz/3pdz.brokenAtomCount.pdb");
		this.pdb = new PdbReader(pdbPath);
	}
	
	@Test
	public void testHasOnlyFirstModel()  throws Exception {
		pdb.init();
		Map<Integer,PdbAtom> atoms = pdb.getAtoms(0);
		
		assertEquals(1426,atoms.size());
	}

	@Test
	public void testGetAtom()  throws Exception {
		pdb.init();
		Map<Integer,PdbAtom> atoms = pdb.getAtoms(0);
		
		PdbAtom atom = atoms.get(2);
		
		assertEquals(-22.132,atom.getX(),0.001);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFailsForBrokenAtomCounts()  throws Exception {
		PdbReader pdb = new PdbReader(brokenPdbPath);
		pdb.init();
	}
	
}
