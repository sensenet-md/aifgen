package importer.timeline;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tcb.aifgen.importer.timeline.StringListTimelineFactory;
import com.tcb.atoms.interactions.Timeline;

public class StringListTimelineFactoryTest {
	
	private StringListTimelineFactory fac;

	@Before
	public void setUp() throws Exception {
		this.fac = new StringListTimelineFactory();
	}

	@Test
	public void testCreate() {
		Timeline ref = Timeline.create(new int[]{1,0,1,0,0,1});
		String timeline = "1 0 1 0 0 1";
		Timeline test = fac.create(timeline);
		
		assertEquals(ref,test);
	}
	
	@Test
	public void testCreateWithWhitespace() {
		Timeline ref = Timeline.create(new int[]{1,0,1,0,0,1});
		String timeline = " 1   0 1   0 0 1  ";
		Timeline test = fac.create(timeline);
		
		assertEquals(ref,test);
	}
	
	@Test
	public void testCreateEmpty() {
		Timeline ref = Timeline.create(new int[]{});
		String timeline = "";
		Timeline test = fac.create(timeline);
		
		assertEquals(ref,test);
		
	}
	
	@Test
	public void testCreateEmptyWhitespace() {
		Timeline ref = Timeline.create(new int[]{});
		String timeline = " ";
		Timeline test = fac.create(timeline);
		
		assertEquals(ref,test);
		
	}

}
