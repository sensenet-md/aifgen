package importer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.tcb.common.util.ListFilter;
import com.tcb.common.util.SafeMap;
import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.InteractionList;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;

import importerTestData.RefData;

public abstract class AbstractImporterTest {

	protected InteractionImporter importer;
	protected InteractionImportData imported;
	private RefData refData;
	private Map<String,Interaction> interactionsMap;
	protected List<Interaction> interactions;
	protected Path dummyFilePath;
	
	public abstract InteractionImporter createImporter();
	
	public abstract InteractionImporter createImporterWithCustomInteractionType(String type);
	public abstract RefData getRefData();
	
	@Test
	public abstract void testGetName();
	
	@Before
	public void setUp() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		this.dummyFilePath = PathUtil.fromResource(classLoader, "atomInteractionList/ImporterTestData/dummy.txt");
		this.refData = getRefData();
		this.importer = createImporter();
		this.imported = importer.read();
		this.interactions = imported.getInteractions();
		
		interactionsMap = interactions.stream()
				.collect(Collectors.toMap( 
						i -> i.toString(),
						i -> i,
						(a,b) -> {throw new RuntimeException(a + " " + b);},
						SafeMap::new));
	}
	
	public InteractionImportData readImporter() throws IOException {
		return importer.read();
	}
		
	@Test
	public void gettingValuesShouldSucceedAfterInitialization() {
		Interaction interaction = refData.getTestInteraction();
		assertTrue(interactions.contains(interaction));
	}
	
	@Test
	public void testNoDoubleInteractions(){
		for(Interaction interaction: interactionsMap.values()){
			Atom source = interaction.getSourceAtom();
			Atom target = interaction.getTargetAtom();
			List<Atom> bridgingAtoms = interaction.getBridgingAtoms();
			String key = interaction.toString();
			String reverseKey = Interaction.create(
					target, source, bridgingAtoms, interaction.getTimeline(), interaction.getType()).toString();
			if(interactionsMap.containsKey(reverseKey)){
				System.out.println(String.format("key: %s reverse key: %s", key,reverseKey));
				fail("Reverse key found");
			}
		}
		
	}
	
	@Test
	public void testGetInteractions() {
		assertEquals(refData.getNumberOfInteractions(),interactions.size());
	}
		
	
	@Test
	public void testGetInteractingAtoms() {
		Set<Atom> interactingAtoms = InteractionList.getInteractingAtoms(interactions);
		
		// TODO Easier test with better reference
		assertEquals(refData.getNumberOfInteractingAtoms(),interactingAtoms.size());
		
		List<Atom> hydrogens = interactingAtoms.stream()
				.filter(a -> a.getName().charAt(0) == 'H')
				.collect(Collectors.toList());
		
		assertEquals(0,hydrogens.size());
	}
	
	@Test
	public void testGetInteractingResidues() {
		Set<Residue> interactingResidues = InteractionList.getInteractingResidues(interactions);
		
		// TODO Easier test with better reference
		assertEquals(refData.getNumberOfInteractingResidues(),interactingResidues.size());
	}
	
	@Test
	public void testGetTimelineLength(){
		int length = InteractionList.getTimelineLength(interactions);
		
		assertEquals(refData.getTimelineLength(),length);
	}
	
	@Test
	public void testGetChains(){
		Set<String> chains = InteractionList.getChains(interactions);
		
		assertEquals(refData.getNumberOfChains(), chains.size());
	}
	
		
	protected List<String> getSortedInteractionKeys(Map<String,Interaction> interactions){
		return interactions.keySet().stream()
				.sorted()
				.collect(Collectors.toList());
	}
	
	protected void assertListsEqual(List<?> list1, List<?> list2){
		assertTrue(list1.size()>0);
		assertEquals(list1.size(),list2.size());
		for(int i=0;i<list1.size();i++){
			assertEquals(list1.get(i),list2.get(i));
		}
	}
		
	@Test
	public void testMutationMapContents() {
		assertTrue(imported.getMutationMap().isEmpty());
	}
	
	@Test
	public void testGetInteractionTypes(){
		assertEquals(refData.getInteractionTypes(), InteractionList.getInteractionTypes(interactions));
	}
	
	@Test
	public void testGetSecondaryStructureMapContents(){
		assertEquals(refData.getNumberOfSecondaryStructures(), imported.getSecondaryStructureMap().size());
		if(refData.getTestSecondaryStructureResidueKey()==null){
			assertTrue(imported.getSecondaryStructureMap().isEmpty()); 
		} else {			
			String secStructResidue = imported.getSecondaryStructureMap().get(
					refData.getTestSecondaryStructureResidueKey());
			assertEquals(refData.getTestSecondaryStructure(), secStructResidue);
		}
				
	}
	
	@Test
	public void testWithCustomInteractionType() throws Exception {
		InteractionImporter testImporter = createImporterWithCustomInteractionType("customType");
				
		for(Interaction i:testImporter.read().getInteractions()){
			assertEquals("customType",i.getType());
		}
	}
	
	protected Set<String> getAtomNames(InteractionImportData imported){
		Set<Atom> atoms = InteractionList.getInteractingAtoms(imported.getInteractions());
		Set<String> atomNames = atoms.stream()
				.map(a -> a.getName())
				.collect(Collectors.toSet());
		return atomNames;
	}

	
}
