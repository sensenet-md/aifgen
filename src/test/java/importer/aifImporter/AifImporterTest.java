package importer.aifImporter;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import importerTestData.AifExampleData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifImporter;
import com.tcb.aifgen.importer.aifImporter.OldMatImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.amberImporter.AmberNativeContactsImporter;
import com.tcb.aifgen.importer.timeline.StringListTimelineFactory;
import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

import importer.AbstractImporterTest;

public class AifImporterTest extends AbstractImporterTest {

	protected ClassLoader classLoader = getClass().getClassLoader();
	protected AifExampleData refData = new AifExampleData();
	
	private Path aifPath;

	@Before
	public void setUp() throws Exception {
		aifPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "test.aif");
		super.setUp();
	}
	
	@Override
	public InteractionImporter createImporter() {
		return new AifImporter(aifPath);
	}

	@Override
	public RefData getRefData() {
		return refData;
	}
	
	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"test.aif");
	}

	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void testWithCustomInteractionType(){
		// Ignore
	}
	
	@Test
	public void testContainsBridgedInteraction(){
		Atom source = Atom.create("CB",141, "ARG", "X", "C", "A");
		Atom target = Atom.create("CG",143, "ASP", "Y", "D", "A");
		Timeline timeline = new StringListTimelineFactory().create("1 1 1 1 0 0 0 0 1 0 1 1");
		Atom b1 = Atom.create("H1", source.getResidue());
		Atom b2 = Atom.create("H2", source.getResidue());
		Atom b3 = Atom.create("H3", source.getResidue());
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(b1,b2,b3),
				timeline,
				InteractionType.CONTACT.name());
		
		assertTrue(interactions.contains(i));
	}
	
	@Test
	public void testContainsUnbridgedInteraction(){
		Atom source = Atom.create("CB",141, "ARG", "X", "C", "A");
		Atom target = Atom.create("CG",143, "ASP", "Y", "D", "A");
		Timeline timeline = new StringListTimelineFactory().create("1 1 1 1 0 0 0 0 1 0 1 1");
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(),
				timeline,
				InteractionType.CONTACT.name());
		
		assertTrue(interactions.contains(i));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFailsForDoubleVersion() throws Exception{
		Path aifPath = 
				PathUtil.fromResource(classLoader, refData.getBaseResourcePath() +
						"test.doubleVersion.aif");
		new AifImporter(aifPath).read();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFailsForTooHighVersion() throws Exception{
		Path aifPath = 
				PathUtil.fromResource(classLoader, refData.getBaseResourcePath() +
						"test.highVersion.aif");
		new AifImporter(aifPath).read();
	}
	
	@Test
	public void testSieve()  throws Exception {
		Integer sieve = 2;
		Double minAvg = Double.NEGATIVE_INFINITY;
		
		InteractionImporter testImporter = 
				new AifImporter(aifPath,sieve,minAvg);
		
		for(Interaction in:testImporter.read().getInteractions()){
			assertEquals(6,(int)in.getTimeline().getLength());
		}

	}
	
	@Test
	public void testMinAvg()  throws Exception {
		Integer sieve = 1;
		Double minAvg = 0.8;
		
		InteractionImporter testImporter = 
				new AifImporter(aifPath,sieve,minAvg);
		
		List<Interaction> interactions = testImporter.read().getInteractions();
		
		assertEquals(1,(int)interactions.size());
		
		for(Interaction in:interactions){
			float sum = 0f;
			for(double d:in.getTimeline().getData()) sum += d;
			double avg = sum / in.getTimeline().getLength();
			assertTrue(avg >= minAvg);
		}
		
	}
}
