package importer.aifImporter;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import importerTestData.AifExampleData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifImporter;
import com.tcb.aifgen.importer.aifImporter.OldMatImporter;
import com.tcb.aifgen.importer.aifImporter.ZaifImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.timeline.StringListTimelineFactory;
import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

import importer.AbstractImporterTest;

public class ZaifImporterTest extends AifImporterTest {
	
	private Path aifPath;
	
	@Before
	public void setUp() throws Exception {
		aifPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "test.zaif");
		super.setUp();
	}
	
	@Override
	public InteractionImporter createImporter() {
		return new ZaifImporter(aifPath);
	}
	
	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"test.zaif");
	}
	
	@Test
	public void testSkip()  throws Exception {
		Integer sieve = 2;
		Double minAvg = Double.NEGATIVE_INFINITY;
		
		InteractionImporter testImporter = 
				new ZaifImporter(aifPath,sieve,minAvg);
		
		for(Interaction in:testImporter.read().getInteractions()){
			assertEquals(6,(int)in.getTimeline().getLength());
		}

	}
	
	@Test
	public void testMinAvg()  throws Exception {
		Integer sieve = 1;
		Double minAvg = 0.8;
		
		InteractionImporter testImporter = 
				new ZaifImporter(aifPath,sieve,minAvg);
		
		List<Interaction> interactions = testImporter.read().getInteractions();
		
		assertEquals(1,(int)interactions.size());
		
		for(Interaction in:interactions){
			float sum = 0f;
			for(double d:in.getTimeline().getData()) sum += d;
			double avg = sum / in.getTimeline().getLength();
			assertTrue(avg >= minAvg);
		}
		
	}
		
}
