package importer.aifImporter;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;
import importerTestData.UhrfTrajHbondsDataMat;

import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.OldMatImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.residues.Residue;

import importer.AbstractImporterTest;

public class OldMatImporterTest extends AbstractImporterTest {

	private Path matPath;
	private UhrfTrajHbondsData refData;


	@Before
	public void setUp() throws Exception {
		this.refData = new UhrfTrajHbondsDataMat();
		ClassLoader classLoader = getClass().getClassLoader();
		this.matPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "hbonds.mat");
		super.setUp();
	}
	
	@Override
	public InteractionImporter createImporter() {
		return new OldMatImporter(matPath);
	}

	@Override
	public RefData getRefData() {
		return refData;
	}
	
	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"hbonds.mat");
	}

	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void testWithCustomInteractionType(){
		// Ignore
	}

}
