package importer.aifImporter;

import static org.junit.Assert.*;

import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tcb.aifgen.importer.InteractionImportData;
import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifImporter;
import com.tcb.aifgen.importer.aifImporter.AifWriter;
import com.tcb.aifgen.util.PathUtil;

import importerTestData.RefData;
import importerTestData.AifExampleData;

public class AifWriterTest extends AifImporterTest {

	private RefData refData;
	private Path path;
	private Path testPath;
	private InteractionImportData ref;

	@Before
	public void setUp() throws Exception {
		this.refData = new AifExampleData();
		ClassLoader classLoader = getClass().getClassLoader();
		this.path = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + "test.aif");
		this.testPath = Files.createTempFile("test", ".aif");
		this.ref = new AifImporter(path).read();
				
		AifWriter writer = new AifWriter(ref.getInteractions(), ref.getTimelineType());
		writer.write(testPath);
				
		super.setUp();
	}
	
	@After
	public void tearDown() throws Exception {
		Files.delete(testPath);
	}
	
	@Override
	public InteractionImporter createImporter() {
		return new AifImporter(testPath);
	}
	
	@Override
	public void testGetName(){
		assertEquals(testPath.getFileName().toString(), importer.getName());
	}

}
