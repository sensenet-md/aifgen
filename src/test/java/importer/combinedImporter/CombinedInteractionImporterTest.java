package importer.combinedImporter;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.aifImporter.AifImporter;
import com.tcb.aifgen.importer.aifImporter.OldMatImporter;
import com.tcb.aifgen.importer.combinedImporter.CombinedInteractionImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbContactImporter;
import com.tcb.aifgen.importer.pdbImporter.PdbHbondImporter;
import com.tcb.aifgen.util.PathUtil;

import importerTestData.DifferenceUhrfTrajHbondsData;
import importerTestData.DnaKPdbCombinedHbondsContactsData;
import importerTestData.AifExampleData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;
import importer.AbstractImporterTest;

public class CombinedInteractionImporterTest extends AbstractImporterTest {
	private RefData refData;
	private RefData trajData1;
	private RefData trajData2;
	private Path contactsPath;
	private Path hbondsPath;
	private Path trajData1Path;
	private Path trajData2Path;
	
	@Before
	public void setUp() throws Exception {
		this.refData = new DnaKPdbCombinedHbondsContactsData();
		this.trajData1 = new AifExampleData();
		this.trajData2 = new UhrfTrajHbondsData();
		ClassLoader classLoader = getClass().getClassLoader();
		this.contactsPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + 
				"contacts.pdb");
		this.hbondsPath = PathUtil.fromResource(classLoader, refData.getBaseResourcePath() + 
				"DnaK-ATP.pdb");
		this.trajData1Path = PathUtil.fromResource(classLoader, trajData1.getBaseResourcePath() + "test.aif");
		this.trajData2Path = PathUtil.fromResource(classLoader, trajData2.getBaseResourcePath() + "hbonds.mat");
		super.setUp();
	}
	
	@Override
	public InteractionImporter createImporter() {
		return new CombinedInteractionImporter(Arrays.asList(
				new PdbContactImporter(contactsPath, 3.0,"O.*|N.*"),
				new PdbHbondImporter(hbondsPath, 3.0,120.0,"F.*|O.*|N.*","F.*|O.*|N.*")
				));
	}
	
	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		Set<String> ignoredAtoms = new HashSet<>();
		return new CombinedInteractionImporter(Arrays.asList(
				new PdbContactImporter(contactsPath, 3.0,"O.*|N.*",type,ignoredAtoms,false),
				new PdbHbondImporter(hbondsPath, 3.0,120.0,"F.*|O.*|N.*","F.*|O.*|N.*",type,new HashSet<>())
				));
	}

	@Override
	public RefData getRefData() {
		return refData;
	}
	
	@Test
	public void testGetName() {
		assertEquals("(contacts.pdb+DnaK-ATP.pdb)", importer.getName());
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testFailsForCombineDifferentLengthTimelines()  throws Exception {
		InteractionImporter importer1 = new AifImporter(trajData1Path);
		InteractionImporter importer2 = new OldMatImporter(trajData2Path);
		InteractionImporter comb = new CombinedInteractionImporter(Arrays.asList(importer1,importer2));
		comb.read(); // Throws
	}
	
	@Test
	public void testSuceedsForSingleImporter()  throws Exception {
		InteractionImporter importer1 = new AifImporter(trajData1Path);
		InteractionImporter comb = new CombinedInteractionImporter(Arrays.asList(importer1));
		comb.read();
	}
	
	@Test
	public void testSuceedsForMixedImporterTimelineLengths()  throws Exception {
		InteractionImporter importer1 = new AifImporter(trajData1Path);
		InteractionImporter importer2 = new PdbContactImporter(contactsPath, 3.0,"O.*|N.*");
		InteractionImporter comb = new CombinedInteractionImporter(Arrays.asList(importer1,importer2));
		comb.read();
	}

	
	

}
