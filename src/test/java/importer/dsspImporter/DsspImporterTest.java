package importer.dsspImporter;

import static org.junit.Assert.*;

import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import importerTestData.DnaKPdbContactsData;
import importerTestData.DsspData;
import importerTestData.RefData;
import importerTestData.UhrfTrajHbondsData;

import com.tcb.aifgen.importer.InteractionImporter;
import com.tcb.aifgen.importer.amberImporter.AmberHbondImporter;
import com.tcb.aifgen.importer.amberImporter.AmberNativeContactsImporter;
import com.tcb.aifgen.importer.dsspImporter.DsspImporter;
import com.tcb.aifgen.util.PathUtil;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;

import importer.AbstractImporterTest;

public class DsspImporterTest extends AbstractImporterTest {

	private RefData refData;
	private Path dsspPath;
	
	@Before
	public void setUp() throws Exception {
		this.refData = new DsspData();
		ClassLoader classLoader = getClass().getClassLoader();
		this.dsspPath = PathUtil.fromResource(classLoader,refData.getBaseResourcePath() + "test.dssp");
		super.setUp();
	}

	@Override
	public InteractionImporter createImporter() {
		return new DsspImporter(dsspPath);
	}

	@Override
	public RefData getRefData() {
		return refData;
	}
		
	@Override
	public void testGetName(){
		assertEquals(importer.getName(),"test.dssp");
	}

	@Override
	public InteractionImporter createImporterWithCustomInteractionType(String type) {
		return new DsspImporter(dsspPath,type);
	}
	
		
}
