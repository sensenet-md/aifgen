package importer.dsspImporter;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.tcb.aifgen.importer.dsspImporter.DsspReader;
import com.tcb.aifgen.importer.pdbImporter.PdbReader;
import com.tcb.aifgen.util.PathUtil;

public class DsspReaderTest {

	private Path dsspPath;
	private DsspReader dssp;

	@Before
	public void setUp() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		this.dsspPath = PathUtil.fromResource(classLoader,"atomInteractionList/DsspReader/test.dssp");
		this.dssp = new DsspReader(dsspPath);

	}

	@Test
	public void testReadFirstEntry()  throws Exception {
		List<Map<String,String>> entries = dssp.readEntries();
		Map<String,String> entry = entries.get(0);
		
		String refSeqRes = "1";
		String refPdbSeq = "1";
		
		String refChain = "";
		String refAA = "GLY";
		String refS = "";
		
		assertEquals(refSeqRes, entry.get("SEQRES"));
		assertEquals(refPdbSeq, entry.get("PDBSEQ"));
		assertEquals(refAA, entry.get("AA"));
		assertEquals(refChain, entry.get("CHAIN"));
		assertEquals(refS, entry.get("S"));
		
	}
	
	@Test
	public void testReadThirdEntry()  throws Exception {
		List<Map<String,String>> entries = dssp.readEntries();
		Map<String,String> entry = entries.get(2);
		
		String refSeqRes = "3";
		String refPdbSeq = "3";
		
		String refChain = "";
		String refAA = "ILE";
		String refS = "E";
		
		assertEquals(refSeqRes, entry.get("SEQRES"));
		assertEquals(refPdbSeq, entry.get("PDBSEQ"));
		assertEquals(refAA, entry.get("AA"));
		assertEquals(refChain, entry.get("CHAIN"));
		assertEquals(refS, entry.get("S"));
		
	}
	
	@Test
	public void testReadLastEntry()  throws Exception {
		List<Map<String,String>> entries = dssp.readEntries();
		Map<String,String> entry = entries.get(entries.size()-1);
		
		String refSeqRes = "601";
		String refPdbSeq = "601";
		
		String refChain = "";
		String refAA = "ALA";
		String refS = "";
		
		assertEquals(refSeqRes, entry.get("SEQRES"));
		assertEquals(refPdbSeq, entry.get("PDBSEQ"));
		assertEquals(refAA, entry.get("AA"));
		assertEquals(refChain, entry.get("CHAIN"));
		assertEquals(refS, entry.get("S"));
		
	}

}
