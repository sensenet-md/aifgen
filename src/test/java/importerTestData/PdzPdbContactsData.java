package importerTestData;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class PdzPdbContactsData implements RefData {

	@Override
	public int getNumberOfInteractingAtoms() {
		return 268;
	}

	@Override
	public int getNumberOfInteractingResidues() {
		return 96;
	}

	@Override
	public int getNumberOfChains() {
		return 1;
	}

	@Override
	public int getTimelineLength() {
		return 30;
	}

	@Override
	public Interaction getTestInteraction() {
		Atom source = Atom.create("N",2, "LYS", "", "", "");
		Atom target = Atom.create("O",2, "LYS", "", "", "");
		//Atom bridge = AtomImpl.create("HN", target.getResidue());
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(),
				StringTimelineFactory.create(getTestInteractionTimeline()),
				InteractionType.CONTACT.toString());
		return i;
	}

	@Override
	public String getTestInteractionTimeline() {
		return "111010010000010011001100010010";
	}

	@Override
	public String getTestInteractionSourceAtomName() {
		return "O";
	}

	@Override
	public String getTestInteractionTargetAtomName() {
		return "N";
	}

	@Override
	public int getNumberOfInteractions() {
		return 424;
	}

	@Override
	public String getBaseResourcePath() {
		return "atomInteractionList/ImporterTestData/pdzData/";
	}
	
	@Override
	public Set<String> getInteractionTypes(){
		return Collections.singleton(InteractionType.CONTACT.toString());
	}

	@Override
	public String getTestSecondaryStructure() {
		return null;
	}

	
	@Override
	public Residue getTestSecondaryStructureResidueKey() {
		return null;
	}
	
	@Override
	public int getNumberOfSecondaryStructures() {
		return 0;
	}

}
