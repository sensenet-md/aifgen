package importerTestData;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class DsspData implements RefData {

	@Override
	public int getNumberOfInteractingAtoms() {
		return 409;
	}

	@Override
	public int getNumberOfInteractingResidues() {
		return 409;
	}

	@Override
	public int getNumberOfChains() {
		return 1;
	}

	@Override
	public int getTimelineLength() {
		return 1;
	}

	@Override
	public Interaction getTestInteraction() {
		Atom source = Atom.create("CA",435, "VAL", "", "", "");
		Atom target = Atom.create("CA",436, "THR", "", "", "");
		//Atom bridge = AtomImpl.create("HN", target.getResidue());
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(),
				StringTimelineFactory.create(getTestInteractionTimeline()),
				InteractionType.SECSTRUCT.toString());
		return i;
	}

	@Override
	public String getTestInteractionTimeline() {
		return "1";
	}

	@Override
	public String getTestInteractionSourceAtomName() {
		return "CA";
	}

	@Override
	public String getTestInteractionTargetAtomName() {
		return "CA";
	}

	@Override
	public int getNumberOfInteractions() {
		return 359;
	}

	@Override
	public String getBaseResourcePath() {
		return "atomInteractionList/ImporterTestData/dsspData/";
	}
	
	@Override
	public Set<String> getInteractionTypes(){
		return Collections.singleton(InteractionType.SECSTRUCT.toString());
	}

	@Override
	public String getTestSecondaryStructure() {
		return "E";
	}

	@Override
	public Residue getTestSecondaryStructureResidueKey() {
		return Residue.create(436, "THR", "", "", "");
	}
	
	@Override
	public int getNumberOfSecondaryStructures() {
		return 601;
	}

}
