package importerTestData;

import java.util.Set;

import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.residues.Residue;

public interface RefData {
	public int getNumberOfInteractions();
	public int getNumberOfInteractingAtoms();
	public int getNumberOfInteractingResidues();
	public int getNumberOfChains();
	public int getTimelineLength();
	public Interaction getTestInteraction();
	public String getTestInteractionTimeline();
	public String getTestInteractionSourceAtomName();
	public String getTestInteractionTargetAtomName();
	public String getBaseResourcePath();
	public Set<String> getInteractionTypes();
	public String getTestSecondaryStructure();
	public Residue getTestSecondaryStructureResidueKey();
	public int getNumberOfSecondaryStructures();
}
