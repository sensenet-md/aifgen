package importerTestData;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class UhrfTrajHbondsDataMat extends UhrfTrajHbondsData {

	@Override
	public Interaction getTestInteraction() {
		Atom source = Atom.create("NH2",40, "ARG", "", "", "?");
		Atom target = Atom.create("O2P",232, "GUA", "", "", "?");
		Atom bridge = Atom.create("HH22", source.getResidue());
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(bridge),
				StringTimelineFactory.create(getTestInteractionTimeline()),
				InteractionType.HBOND.toString());
		return i;
	}

}
