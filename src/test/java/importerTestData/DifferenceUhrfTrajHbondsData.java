package importerTestData;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.tcb.aifgen.importer.timeline.DifferenceStringTimelineFactory;
import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class DifferenceUhrfTrajHbondsData implements RefData {

	@Override
	public int getNumberOfInteractingAtoms() {
		return 702;
	}

	@Override
	public int getNumberOfInteractingResidues() {
		return 229;
	}

	@Override
	public int getNumberOfChains() {
		return 1;
	}

	@Override
	public int getTimelineLength() {
		return 201;
	}

	@Override
	public Interaction getTestInteraction() {
		Atom source = Atom.create("N",98, "LEU", "", "", "?");
		Atom target = Atom.create("O", 94, "ARG", "", "", "?");
		Atom bridge = Atom.create("HN", source.getResidue());
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(bridge),
				DifferenceStringTimelineFactory.create(getTestInteractionTimeline()), 
				InteractionType.HBOND.toString());
		return i;
	}

	@Override
	public String getTestInteractionTimeline() {
		return "32331101233303312310333012000012330"
				+ "0313233313333313013133333313303331"
				+ "03313133111103133313331133111003331"
				+ "123133332333313331331231013113211110"
				+ "3313131313012013113113313323131113311"
				+ "330333033133030331110313";
	}

	@Override
	public String getTestInteractionSourceAtomName() {
		return "N";
	}

	@Override
	public String getTestInteractionTargetAtomName() {
		return "O";
	}

	@Override
	public int getNumberOfInteractions() {
		return 1065;
	}

	@Override
	public String getBaseResourcePath() {
		return "atomInteractionList/ImporterTestData/uhrfTrajHbondsData/";
	}
	
	@Override
	public Set<String> getInteractionTypes(){
		return Collections.singleton(InteractionType.HBOND.toString());
	}

	@Override
	public String getTestSecondaryStructure() {
		return null;
	}
	
	@Override
	public Residue getTestSecondaryStructureResidueKey() {
		return null;
	}
	
	@Override
	public int getNumberOfSecondaryStructures() {
		return 0;
	}

}
