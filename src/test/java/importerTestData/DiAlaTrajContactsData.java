package importerTestData;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class DiAlaTrajContactsData implements RefData {

	@Override
	public int getNumberOfInteractingAtoms() {
		return 9;
	}

	@Override
	public int getNumberOfInteractingResidues() {
		return 4;
	}

	@Override
	public int getNumberOfChains() {
		return 1;
	}

	@Override
	public int getTimelineLength() {
		return 50;
	}

	@Override
	public Interaction getTestInteraction() {
		Atom source = Atom.create("C",1, "ACE", "", "", "");
		Atom target = Atom.create("C",2, "ALA", "", "", "");
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(),
				StringTimelineFactory.create(getTestInteractionTimeline()),
				InteractionType.CONTACT.toString());
		return i;
	}

	@Override
	public String getTestInteractionTimeline() {
		return "00000000001010000000010000000000000100000000000000";
	}

	@Override
	public String getTestInteractionSourceAtomName() {
		return "C";
	}

	@Override
	public String getTestInteractionTargetAtomName() {
		return "C";
	}

	@Override
	public int getNumberOfInteractions() {
		return 12;
	}

	@Override
	public String getBaseResourcePath() {
		return "atomInteractionList/ImporterTestData/2alaTrajContactsData/";
	}
	
	@Override
	public Set<String> getInteractionTypes(){
		return Collections.singleton(InteractionType.CONTACT.toString());
	}

	@Override
	public String getTestSecondaryStructure() {
		return null;
	}

	
	@Override
	public Residue getTestSecondaryStructureResidueKey() {
		return null;
	}
	
	@Override
	public int getNumberOfSecondaryStructures() {
		return 0;
	}

}
