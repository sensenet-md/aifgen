package importerTestData;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class UhrfTrajHbondsData implements RefData {

	@Override
	public int getNumberOfInteractingAtoms() {
		return 638;
	}

	@Override
	public int getNumberOfInteractingResidues() {
		return 221;
	}

	@Override
	public int getNumberOfChains() {
		return 1;
	}

	@Override
	public int getTimelineLength() {
		return 201;
	}

	@Override
	public Interaction getTestInteraction() {
		Atom source = Atom.create("NH2",40, "ARG", "", "", "");
		Atom target = Atom.create("O2P",232, "GUA", "", "", "");
		Atom bridge = Atom.create("HH22", source.getResidue());
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(bridge),
				StringTimelineFactory.create(getTestInteractionTimeline()),
				InteractionType.HBOND.toString());
		return i;
	}

	@Override
	public String getTestInteractionTimeline() {
		return "01111111111000000100000000000000000100000000100"
				+ "000000000001111111111111111111111010000000000"
				+ "0000000100000000000000000000000001111110000000"
				+ "00000000000000000000000000000000000000000000001"
				+ "0100100001000001";
	}

	@Override
	public String getTestInteractionSourceAtomName() {
		return "NH2";
	}

	@Override
	public String getTestInteractionTargetAtomName() {
		return "O2P";
	}

	@Override
	public int getNumberOfInteractions() {
		return 761;
	}

	@Override
	public String getBaseResourcePath() {
		return "atomInteractionList/ImporterTestData/uhrfTrajHbondsData/";
	}
	
	@Override
	public Set<String> getInteractionTypes(){
		return Collections.singleton(InteractionType.HBOND.toString());
	}

	@Override
	public String getTestSecondaryStructure() {
		return null;
	}

	@Override
	public Residue getTestSecondaryStructureResidueKey() {
		return null;
	}

	@Override
	public int getNumberOfSecondaryStructures() {
		return 0;
	}

}
