package importerTestData;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class AifExampleData implements RefData {

	@Override
	public int getNumberOfInteractingAtoms() {
		return 4;
	}

	@Override
	public int getNumberOfInteractingResidues() {
		return 2;
	}

	@Override
	public int getNumberOfChains() {
		return 1;
	}

	@Override
	public int getTimelineLength() {
		return 12;
	}

	@Override
	public Interaction getTestInteraction() {
		Atom source = Atom.create("NE",141, "ARG", "X", "C", "A");
		Atom target = Atom.create("OD1",143, "ASP", "Y", "D", "A");
		Atom bridge = Atom.create("HE", source.getResidue());
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(bridge),
				StringTimelineFactory.create(getTestInteractionTimeline()),
				InteractionType.HBOND.name());
		return i;
	}

	@Override
	public String getTestInteractionTimeline() {
		return "111111101011";
	}

	@Override
	public String getTestInteractionSourceAtomName() {
		return "CB";
	}

	@Override
	public String getTestInteractionTargetAtomName() {
		return "CG";
	}

	@Override
	public int getNumberOfInteractions() {
		return 3;
	}

	@Override
	public String getBaseResourcePath() {
		return "atomInteractionList/ImporterTestData/aif/";
	}
	
	@Override
	public Set<String> getInteractionTypes(){
		return new HashSet<String>(Arrays.asList(InteractionType.HBOND.name(),
				InteractionType.CONTACT.name()));
	}

	@Override
	public String getTestSecondaryStructure() {
		return null;
	}

	@Override
	public Residue getTestSecondaryStructureResidueKey() {
		return null;
	}
	
	@Override
	public int getNumberOfSecondaryStructures() {
		return 0;
	}

}
