package importerTestData;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class DnaKPdbContactsData implements RefData {

	@Override
	public int getNumberOfInteractingAtoms() {
		return 1685;
	}

	@Override
	public int getNumberOfInteractingResidues() {
		return 603;
	}

	@Override
	public int getNumberOfChains() {
		return 1;
	}

	@Override
	public int getTimelineLength() {
		return 1;
	}

	@Override
	public Interaction getTestInteraction() {
		Atom source = Atom.create("O",539, "ASP", "", "", "");
		Atom target = Atom.create("N",540, "HIE", "", "", "");
		//Atom bridge = AtomImpl.create("HN", target.getResidue());
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(),
				StringTimelineFactory.create(getTestInteractionTimeline()),
				InteractionType.CONTACT.toString());
		return i;
	}

	@Override
	public String getTestInteractionTimeline() {
		return "1";
	}

	@Override
	public String getTestInteractionSourceAtomName() {
		return "O";
	}

	@Override
	public String getTestInteractionTargetAtomName() {
		return "N";
	}

	@Override
	public int getNumberOfInteractions() {
		return 1859;
	}

	@Override
	public String getBaseResourcePath() {
		return "atomInteractionList/ImporterTestData/DnaKPdbContactsData/";
	}
	
	@Override
	public Set<String> getInteractionTypes(){
		return Collections.singleton(InteractionType.CONTACT.toString());
	}

	@Override
	public String getTestSecondaryStructure() {
		return null;
	}

	
	@Override
	public Residue getTestSecondaryStructureResidueKey() {
		return null;
	}
	
	@Override
	public int getNumberOfSecondaryStructures() {
		return 0;
	}

}
