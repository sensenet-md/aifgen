package importerTestData;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.tcb.aifgen.importer.timeline.StringTimelineFactory;
import com.tcb.atoms.atoms.Atom;
import com.tcb.atoms.interactions.Interaction;
import com.tcb.atoms.interactions.InteractionType;
import com.tcb.atoms.interactions.Timeline;
import com.tcb.atoms.residues.Residue;

public class DnaKPdbHbondsData implements RefData {

	@Override
	public int getNumberOfInteractingAtoms() {
		return 607;
	}

	@Override
	public int getNumberOfInteractingResidues() {
		return 395;
	}

	@Override
	public int getNumberOfChains() {
		return 1;
	}

	@Override
	public int getTimelineLength() {
		return 1;
	}

	@Override
	public Interaction getTestInteraction() {
		Atom source = Atom.create("N",116, "ALA", "", "", "");
		Atom target = Atom.create("O",112, "PRO", "", "", "");
		Atom bridge = Atom.create("H", source.getResidue());
		Interaction i = Interaction.create(
				source, target, 
				Arrays.asList(bridge),
				StringTimelineFactory.create(getTestInteractionTimeline()),
				InteractionType.HBOND.toString());
		return i;
	}

	@Override
	public String getTestInteractionTimeline() {
		return "1";
	}

	@Override
	public String getTestInteractionSourceAtomName() {
		return "N";
	}

	@Override
	public String getTestInteractionTargetAtomName() {
		return "O";
	}

	@Override
	public int getNumberOfInteractions() {
		return 312;
	}

	@Override
	public String getBaseResourcePath() {
		return "atomInteractionList/ImporterTestData/DnaKPdbHbondsData/";
	}
	
	@Override
	public Set<String> getInteractionTypes(){
		return Collections.singleton(InteractionType.HBOND.toString());
	}

	@Override
	public String getTestSecondaryStructure() {
		return null;
	}
	
	@Override
	public Residue getTestSecondaryStructureResidueKey() {
		return null;
	}
	
	@Override
	public int getNumberOfSecondaryStructures() {
		return 0;
	}

}
