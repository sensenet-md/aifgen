#!/bin/bash

set -eux
set -o pipefail

mvn install

Bin=$(ls -1 -rt target/*-jar-with-dependencies.jar | tail -n1)
Base=$(basename ${Bin} -jar-with-dependencies.jar)
TmpDir=publish-tmp

rm -rf ${TmpDir}
mkdir ${TmpDir}

cp ${Bin} ${TmpDir}/${Base}.jar
cp doc/README ${TmpDir}/
cp target/generated-sources/license/LICENSE.txt ${TmpDir}/
cp target/generated-sources/license/THIRD-PARTY.txt ${TmpDir}/

cd ${TmpDir}
zip ${Base}.zip *
cd -
cp ${TmpDir}/${Base}.zip target/

rm -rf ${TmpDir}
