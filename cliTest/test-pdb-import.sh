#!/bin/bash

set -eux
set -o pipefail

Bin=$(ls -tr ../target/*.jar | tail -n 1)

InPdb=./resources/pdb/pdz.pdb

RefAif=resources/pdb/pdz.ref.aif
OutFile=out/pdz.aif

java -jar ${Bin} \
        import_pdb_hbonds \
        --pdb ${InPdb} \
        --distance 4.0 \
        --angle 120 \
        --donorPattern 'O.*|N.*' \
        --acceptorPattern 'O.*|N.*' \
        --ignoreAtomNames 'O,N' \
        import_pdb_contacts \
        --pdb ${InPdb} \
        --ignoreAtomNames 'C,CA' \
        --distance 4.0 \
        --atomPattern 'C.*' \
        --ignoreIntraResidue \
        export_aif \
        -o ${OutFile} 
sed -i '1d' ${OutFile}
sort ${OutFile} -o ${OutFile}
diff ${RefAif} ${OutFile}

Error=$?

if [[ ${Error} -eq 0 ]]
then
        echo "OK"
else
        echo "Error: Exported .aif files not equal"
fi
