#!/bin/bash

set -eux
set -o pipefail

Bin=$(ls -tr ../target/*.jar | tail -n 1)

ResourceZips=$(find ./resources -name "*.zip")
for Zip in ${ResourceZips}
do
        Dir=$(dirname ${Zip})
        unzip -n -d ${Dir} ${Zip}
done

RefAif=./resources/aif/test.aif
ContactBaseDir=./resources/cpptraj/nativecontacts
ContactOut=${ContactBaseDir}/test.out
ContactNativeSeries=${ContactBaseDir}/test.native.series
ContactNonNativeSeries=${ContactBaseDir}/test.nonnative.series
ContactPdb=${ContactBaseDir}/test.pdb
HbondBaseDir=./resources/cpptraj/hbonds
HbondOut=${HbondBaseDir}/test.out
HbondSeries=${HbondBaseDir}/test.series


OutFile=out/test.out.aif

java -jar ${Bin} \
        import_cpptraj_hbonds \
        --out ${HbondOut} \
        --series ${HbondSeries} \
	--sieve 1 \
	--minAvg 0.00 \
        import_cpptraj_contacts \
        --out ${ContactOut} \
        --nativeSeries ${ContactNativeSeries} \
        --nonNativeSeries ${ContactNonNativeSeries} \
        --pdb ${ContactPdb} \
        --ignoreIntraResidue \
	--sieve 1 \
	--minAvg 0.00 \
        export_aif \
        -o ${OutFile}
sed -i '1d' ${OutFile}
sort ${OutFile} -o ${OutFile}
diff ${RefAif} ${OutFile}

Error=$?

if [[ ${Error} -eq 0 ]]
then
        echo "OK"
else
        echo "Error: Exported .aif files not equal"
fi
